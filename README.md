# t1h0（原icy2003）的 PHP 库

一个工具集，它功能丰富、执行高效、注释完整、使用简单

![Packagist Downloads](https://img.shields.io/packagist/dt/t1h0/php)
![Packagist License](https://img.shields.io/packagist/l/t1h0/php)

## 功能

包括但不限于以下列表：

1. 数组字符串的各种操作
2. 本地、ftp、sftp 等的文件操作，例如：批量删除、添加、复制、剪切文件（夹）
3. 编码转换、时间转换、请求处理、缓存、文本处理、数学计算、图片处理、常用正则等
4. 针对 yii2、phpoffice 等的扩展
5. 微信支付宝的支付、退款等
6. 百度 AI 接口

## 传送门

- [gitee](https://gitee.com/t1h0/php)
- [packagist](https://packagist.org/packages/t1h0/php)
- [phpdoc](https://apidoc.gitee.com/t1h0/php)

## 安装

```cmd
composer require t1h0/php
```

## 关于 [icy2003/php](https://github.com/icy2003/php)

从github 迁移至 gitee，github 上的不再维护！

- 如果想继续使用 v1 版本，可以使用 v2 版本的 v1 分支，命令为：`composer require t1h0/php ^v1.x-dev`，该版本只会修复一些明显的 bug 而不会增加新功能
- v2 版本不再兼容 v1 版本，PHP 版本会升级到 PHP7+
