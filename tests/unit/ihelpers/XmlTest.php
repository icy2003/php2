<?php

namespace t1h0\php_tests\ihelpers;

use t1h0\php\ihelpers\Xml;

class XmlTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $_string = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<object>
    <name>t1h0</name>
</object>
EOT;

    public function testIsXml()
    {
        $this->tester->assertTrue(Xml::isXml($this->_string));
        $this->tester->assertFalse(Xml::isXml($this->_string . 'x'));
    }

    public function testToArray()
    {
        $this->tester->assertEquals(Xml::toArray($this->_string), [
            'name' => 't1h0',
        ]);
        $this->tester->assertEquals(Xml::toArray($this->_string . 'x'), []);
    }

    public function testFromArray()
    {
        $this->tester->assertEquals(Xml::fromArray([
            'name' => 't1h0',
            'number' => 2003,
            'info' => [
                'web' => 'https://www.t1h0.com',
            ],
        ]), '<xml><name><![CDATA[t1h0]]></name><number>2003</number><info><web><![CDATA[https://www.t1h0.com]]></web></info></xml>');
    }
}
