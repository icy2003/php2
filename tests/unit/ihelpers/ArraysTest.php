<?php

namespace t1h0\php_tests\ihelpers;

use t1h0\php\I;
use t1h0\php\ihelpers\ArrayStatic;

class ArrayStaticTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testIndexBy()
    {
        $this->tester->assertEquals(ArrayStatic::indexBy(
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ],
            'id'
        ),
            [
                1 => ['id' => 1, 'name' => 'tom'],
                2 => ['id' => 2, 'name' => 'jerry'],
            ]
        );
        $this->tester->assertEquals(ArrayStatic::indexBy(
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
                ['id' => 2, 'name' => 'jerry2'],
            ],
            'id',
            true
        ),
            [
                1 => [
                    ['id' => 1, 'name' => 'tom'],
                ],
                2 => [
                    ['id' => 2, 'name' => 'jerry'],
                    ['id' => 2, 'name' => 'jerry2'],
                ],
            ]
        );
        $this->tester->assertEquals(ArrayStatic::indexBy([
            ['id' => 1, 'name' => 'tom'],
        ], 'type'), []);
    }

    public function testColumns()
    {
        $this->tester->assertEquals(ArrayStatic::columns(
            [
                ['id' => 1, 'name' => 'tom', 'type' => 'cat'],
                ['id' => 2, 'name' => 'jerry', 'type' => 'rat'],
            ],
            ['id', 'name'],
            2
        ),
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ]
        );
        $this->tester->assertEquals(ArrayStatic::columns(
            ['id' => 1, 'name' => 'tom', 'type' => 'cat'],
            ['id', 'name'],
            1
        ), ['id' => 1, 'name' => 'tom']);
        $this->tester->assertEquals(ArrayStatic::columns(['a', 'b']), ['a', 'b']);
        $this->tester->assertEquals(ArrayStatic::columns(
            [
                ['id' => 1, 'name' => 'tom', 'type' => 'cat'],
                ['id' => 2, 'name' => 'jerry', 'type' => 'rat'],
            ],
            ['id1'],
            2
        ),
            [
                ['id1' => null],
                ['id1' => null],
            ]
        );
    }

    public function testColumn()
    {
        I::ini('USE_CUSTOM', false);
        $this->tester->assertEquals(ArrayStatic::column(
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ],
            'name'
        ),
            ['tom', 'jerry']
        );
        $this->tester->assertEquals(ArrayStatic::column(
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ],
            'name',
            'id'
        ),
            [1 => 'tom', 2 => 'jerry']
        );
        I::ini('USE_CUSTOM', true);
        $this->tester->assertEquals(ArrayStatic::column(
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ],
            'name',
            'id'
        ),
            [1 => 'tom', 2 => 'jerry']
        );
        $this->tester->assertEquals(ArrayStatic::column(
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ],
            'name',
            null
        ),
            [0 => 'tom', 1 => 'jerry']
        );
        I::ini('USE_CUSTOM', false);
    }

    public function testCombine()
    {
        $this->tester->assertEquals(ArrayStatic::combine(
            ['id', 'name'],
            [2, 'tom']
        ),
            ['id' => 2, 'name' => 'tom']);
        $this->tester->assertEquals(ArrayStatic::combine(
            ['id'],
            [2, 'tom']
        ),
            ['id' => 2]);
    }

    public function testMerge()
    {
        $this->tester->assertEquals(ArrayStatic::merge(
            ['tom'],
            ['jerry']
        ),
            ['tom', 'jerry']
        );
        $this->tester->assertEquals(ArrayStatic::merge(
            ['tom'],
            ['jerry'],
            ['speike']
        ),
            ['tom', 'jerry', 'speike']
        );
        $this->tester->assertEquals(ArrayStatic::merge(
            ['name' => 'tom'],
            ['name' => 'jerry']
        ),
            ['name' => 'jerry']
        );
        $this->tester->assertEquals(ArrayStatic::merge(
            ['name' => 'tom', 'attr' => [
                'height' => '1m',
            ]],
            ['name' => 'jerry', 'attr' => [
                'color' => 'blue',
            ]]
        ),
            ['name' => 'jerry', 'attr' => [
                'height' => '1m',
                'color' => 'blue',
            ]]
        );
    }

    public function testRangeGenerator()
    {
        $array1 = [];
        foreach (ArrayStatic::rangeGenerator(1, 3) as $value) {
            $array1[] = $value;
        }
        $array2 = [];
        foreach (ArrayStatic::rangeGenerator(5, 1, -2) as $value) {
            $array2[] = $value;
        }
        $this->tester->assertEquals($array1, [1, 2, 3]);
        $this->tester->assertEquals($array2, [5, 3, 1]);
        try {
            foreach (ArrayStatic::rangeGenerator(1, 5, -1) as $value) {
            }
        } catch (\Exception $e) {
            $this->tester->assertTrue(true);
        }
        try {
            foreach (ArrayStatic::rangeGenerator(5, 1, 1) as $value) {
            }
        } catch (\Exception $e) {
            $this->tester->assertTrue(true);
        }
    }

    public function testDetectFirst()
    {
        $this->tester->assertEquals(ArrayStatic::detectFirst(
            [1, 2, 3],
            function ($v) {
                return $v > 1;
            }
        ),
            2
        );
        $this->tester->assertEquals(ArrayStatic::detectFirst(
            [1, 2, 3],
            function ($v) {
                return $v > 5;
            }
        ),
            null
        );
    }

    public function testDetectAll()
    {
        $this->tester->assertEquals(ArrayStatic::detectAll(
            [1, 2, 3],
            function ($v) {
                return $v > 1;
            }
        ),
            [1 => 2, 2 => 3]
        );
        $this->tester->assertEquals(ArrayStatic::detectAll(
            [1, 2, 3],
            function ($v) {
                return $v > 1;
            },
            function ($v) {
                return $v + 1;
            }
        ),
            [1 => 3, 2 => 4]
        );
    }

    public function testKeyLast()
    {
        I::ini('USE_CUSTOM', false);
        $this->tester->assertEquals(ArrayStatic::keyLast(['id' => 1, 'name' => 'tom']), 'name');
        $this->tester->assertEquals(ArrayStatic::keyLast(1), null);
        I::ini('USE_CUSTOM', true);
        $this->tester->assertEquals(ArrayStatic::keyLast(['id' => 1, 'name' => 'tom']), 'name');
        I::ini('USE_CUSTOM', false);
    }

    public function testKeyFirst()
    {
        I::ini('USE_CUSTOM', false);
        $this->tester->assertEquals(ArrayStatic::keyFirst([1, 2, 3]), 0);
        $this->tester->assertEquals(ArrayStatic::keyFirst(1), null);
        I::ini('USE_CUSTOM', true);
        $this->tester->assertEquals(ArrayStatic::keyFirst([1, 2, 3]), 0);
        I::ini('USE_CUSTOM', false);
    }

    public function testDimension()
    {
        $this->tester->assertEquals(ArrayStatic::dimension([1, 2]), 1);
        $this->tester->assertEquals(ArrayStatic::dimension(
            [
                [
                    'name' => 'tom',
                    'ability' => [
                        'fly' => true,
                    ],
                ],
                [
                    'name' => 'jerry',
                ],
            ]
        ),
            3
        );
        $this->tester->assertEquals(ArrayStatic::dimension(1), 0);
    }

    public function testIsAssoc()
    {
        $this->tester->assertTrue(ArrayStatic::isAssoc(['name' => 'tom']));
        $this->tester->assertFalse(ArrayStatic::isAssoc([1, 2]));
        $this->tester->assertFalse(ArrayStatic::isAssoc(1));
    }

    public function testIsIndexed()
    {
        $this->tester->assertFalse(ArrayStatic::isIndexed(['name' => 'tom']));
        $this->tester->assertTrue(ArrayStatic::isIndexed([1, 2]));
        $this->tester->assertFalse(ArrayStatic::isIndexed(1));
    }

    public function testFirst()
    {
        $this->tester->assertEquals(ArrayStatic::first([1, 2, 3]), 1);
        $this->tester->assertEquals(ArrayStatic::first([1, 2, 3], 3), 3);
        $this->tester->assertEquals(ArrayStatic::first([]), null);
        $this->tester->assertEquals(ArrayStatic::first(
            [
                ['name' => 'tom'],
                ['name' => 'jerry'],
            ],
            2
        ),
            ['name' => 'jerry']
        );
    }

    public function testLast()
    {
        $this->tester->assertEquals(ArrayStatic::last([1, 2, 3]), 3);
        $this->tester->assertEquals(ArrayStatic::last([1, 2, 3], 3), 1);
        $this->tester->assertEquals(ArrayStatic::last([]), null);
        $this->tester->assertEquals(ArrayStatic::last(
            [
                ['name' => 'tom'],
                ['name' => 'jerry'],
            ],
            2
        ),
            ['name' => 'tom']
        );
    }

    public function testCount()
    {
        $this->tester->assertEquals(ArrayStatic::count(0), 0);
        $this->tester->assertEquals(ArrayStatic::count([1, 2]), 2);
        $this->tester->assertEquals(ArrayStatic::count([1, 2, 3, 4], 2), 1);
        $this->tester->assertEquals(ArrayStatic::count([1, 2, 3, 4], '2', false), 1);
    }

    public function testLists()
    {
        $this->tester->assertEquals(ArrayStatic::lists([1, 2, 3], 4), [1, 2, 3, null]);
        $this->tester->assertEquals(ArrayStatic::lists([1, 2, 3], 2), [1, 2, 3]);
        $this->tester->assertEquals(ArrayStatic::lists(['a' => '2aa', 'b' => 'b4b'], 2, 'trim'), ['a' => 2, 'b' => 4]);
    }

    public function testValues()
    {
        $this->tester->assertEquals(ArrayStatic::values(
            [
                'id' => 1,
                'name' => 'tom',
                'type' => 'cat',
            ],
            'id,type'
        ), [1, 'cat']);
        $this->tester->assertEquals(ArrayStatic::values(
            [
                'id' => 1,
                'name' => 'tom',
                'type' => 'cat',
            ],
            ['id', 'type']
        ), [1, 'cat']);
    }

    public function testSome()
    {
        $this->tester->assertEquals(ArrayStatic::some(
            [
                'id' => 1,
                'name' => 'tom',
                'type' => 'cat',
            ],
            'id,type'
        ), ['id' => 1, 'type' => 'cat']);
        $this->tester->assertEquals(ArrayStatic::some(
            [
                'id' => 1,
                'name' => 'tom',
                'type' => 'cat',
            ],
            ['id', 'type']
        ), ['id' => 1, 'type' => 'cat']);
        $this->tester->assertEquals(ArrayStatic::some(['a']), ['a']);
    }

    public function testExceptedKeys()
    {
        $this->tester->assertEquals(ArrayStatic::exceptedKeys(
            [
                'id' => 1,
                'name' => 'tom',
                'type' => 'cat',
            ],
            'id,name'
        ), ['type' => 'cat']);
        $this->tester->assertEquals(ArrayStatic::exceptedKeys(
            [
                'id' => 1,
                'name' => 'tom',
                'type' => 'cat',
            ],
            ['id', 'name']
        ), ['type' => 'cat']);
    }

    public function testKeyExistsAll()
    {
        $this->tester->assertTrue(ArrayStatic::keyExistsAll(
            ['id', 'name'],
            ['id' => 1, 'name' => 'tom', 'type' => 'cat']
        ));
        $this->tester->assertFalse(ArrayStatic::keyExistsAll(
            ['image'],
            ['id' => 1, 'name' => 'tom', 'type' => 'cat']
        ));
    }

    public function testKeyExistsSome()
    {
        $this->tester->assertTrue(ArrayStatic::keyExistsSome(
            ['id', 'name', 'name2'],
            ['id' => 1, 'name' => 'tom', 'type' => 'cat']
        ));
        $this->tester->assertFalse(ArrayStatic::keyExistsSome(
            ['image'],
            ['id' => 1, 'name' => 'tom', 'type' => 'cat']
        ));
    }

    public function testValueExistsAll()
    {
        $this->tester->assertTrue(ArrayStatic::valueExistsAll(
            ['name'],
            ['name', 'type']
        ));
        $this->tester->assertFalse(ArrayStatic::valueExistsAll(
            ['name', 'name2'],
            ['name', 'type'],
            $diff
        ));
        $this->tester->assertEquals($diff, [1 => 'name2']);
    }

    public function testValueExistsSome()
    {
        $this->tester->assertTrue(ArrayStatic::valueExistsSome(
            ['name', 'name2'],
            ['name', 'type'],
            $find
        ));
        $this->tester->assertEquals($find, ['name']);
    }

    public function testCombines()
    {
        $this->tester->assertEquals(ArrayStatic::combines(
            ['id', 'name'],
            [
                [1, 'tom'],
                [2, 'jerry'],
            ]
        ),
            [
                ['id' => 1, 'name' => 'tom'],
                ['id' => 2, 'name' => 'jerry'],
            ]
        );
    }

    public function testToPart()
    {
        $this->tester->assertEquals(ArrayStatic::toPart(
            ['1,,4', '3', 2, '4']
        ),
            ['1', '4', '3', '2']
        );
    }

    public function testTransposed()
    {
        $this->tester->assertEquals(ArrayStatic::transposed(
            [
                [1, 2, 3],
                [4, 5, 6],
            ]
        ),
            [
                [1, 4],
                [2, 5],
                [3, 6],
            ]
        );
    }

    public function testToCellArray()
    {
        $this->tester->assertEquals(ArrayStatic::toCellArray(
            [
                [1, 2, 3],
                [4, 5, 6],
            ]
        ),
            [
                1 => ['A' => 1, 'B' => 2, 'C' => 3],
                2 => ['A' => 4, 'B' => 5, 'C' => 6],
            ]
        );
    }

    public function testColRowCount()
    {
        $this->tester->assertEquals(ArrayStatic::colRowCount([
            [1, 2, 3],
            [4, 5, 6],
        ]), [3, 2]);
    }

    public function testFill()
    {
        $this->tester->assertEquals(ArrayStatic::fill(0, 3, 'a'), ['a', 'a', 'a']);
        $this->tester->assertEquals(ArrayStatic::fill(-2, 3, 'a'), [
            '-2' => 'a', '-1' => 'a', '0' => 'a',
        ]);
        $this->tester->assertEquals(ArrayStatic::fill(0, -1, 'a'), []);
    }

    public function testExport()
    {
        $array = ArrayStatic::export(['a']);
        ob_start();
        ArrayStatic::export(['a'], false);
        $temp = ob_get_contents();
        $this->tester->assertEquals($temp, $array);
        ob_end_clean();
    }

    public function testFromCsv()
    {
        $this->tester->assertEquals(ArrayStatic::fromCsv('1,2,3' . PHP_EOL . '4,5,6'), [
            [1, 2, 3],
            [4, 5, 6],
        ]);
    }

    public function testSearch()
    {
        $this->tester->assertEquals(ArrayStatic::search(1, ['a' => 1]), 'a');
        $this->tester->assertEquals(ArrayStatic::search(function ($x) {
            return $x == 1;
        },
            ['a' => '1a']
        ), 'a');
        $this->tester->assertFalse(ArrayStatic::search(function ($x) {
            return $x == 1;
        }, ['a' => 2]));
    }

    public function testIncrement()
    {
        $array = [
            'count' => 1,
        ];
        $this->tester->assertEquals(ArrayStatic::increment($array, 'count', 2), 3);
        $this->tester->assertEquals($array, [
            'count' => 3,
        ]);
    }

    public function testDecrement()
    {
        $array = [
            'count' => 3,
        ];
        $this->tester->assertEquals(ArrayStatic::decrement($array, 'count', 2), 1);
        $this->tester->assertEquals($array, [
            'count' => 1,
        ]);
    }

    public function testIn()
    {
        $this->tester->assertTrue(ArrayStatic::in('1', [1]));
        $this->tester->assertFalse(ArrayStatic::in('1', [1], true));
        $this->tester->assertTrue(ArrayStatic::in('a', ['A'], false, true));
        $this->tester->assertFalse(ArrayStatic::in(1, ''));
    }

}
