<?php
/**
 * Class Mobile
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\iapis;

use t1h0\php\I;
use t1h0\php\ihelpers\Http;
use t1h0\php\ihelpers\Json;

/**
 * 手机号相关接口
 */
class Mobile extends Api
{
    /**
     * 查询手机归属地
     *
     * @param string $number 手机号码
     *
     * @return static
     */
    public function fetchAttribution($number)
    {
        $this->_result = Json::get(Http::get('https://sp0.baidu.com/8aQDcjqpAAV3otqbppnN2DJv/api.php', [
            'resource_name' => 'guishudi',
            'query' => $number,
            'oe' => 'utf8',
        ]), 'data.0', []);
        $this->_toArrayCall = function ($array) {
            return [
                'province' => I::get($array, 'prov'),
                'city' => I::get($array, 'city'),
                'type' => I::get($array, 'type'),
            ];
        };

        return $this;
    }
}
