<?php
/**
 * Class FileConstants
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\icomponents\file;

/**
 * 常量
 */
class FileConstants
{

    /**
     * 使用完整路径
     */
    const COMPLETE_PATH = 1;

    /**
     * 完整路径禁用
     */
    const COMPLETE_PATH_DISABLED = 2;

    /**
     * 递归遍历
     */
    const RECURSIVE = 4;

    /**
     * 递归遍历禁用
     */
    const RECURSIVE_DISABLED = 8;
}
