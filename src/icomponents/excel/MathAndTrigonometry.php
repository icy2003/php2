<?php
/**
 * Trait MathAndTrigonometry
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */

namespace t1h0\php\icomponents\excel;

use t1h0\php\icomponents\excel\mathAndTrigonometry\AMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\BMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\CMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\DMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\EMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\FMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\GMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\IMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\LMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\MMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\OMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\PMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\QMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\RMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\SMAT;
use t1h0\php\icomponents\excel\mathAndTrigonometry\TMAT;

/**
 * 数学和三角
 */
trait MathAndTrigonometry
{
    use AMAT, BMAT, CMAT, DMAT, EMAT, FMAT, GMAT, IMAT, LMAT, MMAT, OMAT, PMAT, QMAT, RMAT, SMAT, TMAT;
}
