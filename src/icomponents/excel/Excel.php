<?php
/**
 * Class Excel
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */

namespace t1h0\php\icomponents\excel;

/**
 * Excel 的函数集
 *
 * - 有生之年系列
 *
 * @see https://support.office.com/zh-cn/article/excel-%E5%87%BD%E6%95%B0%EF%BC%88%E6%8C%89%E7%B1%BB%E5%88%AB%E5%88%97%E5%87%BA%EF%BC%89-5f91f4e9-7b42-46d2-9bd1-63f26a86c0eb
 */
class Excel
{
    use MathAndTrigonometry;
}
