<?php
/**
 * Trait MathAndTrigonometry-Q
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\icomponents\excel\mathAndTrigonometry;

/**
 * MathAndTrigonometry-Q
 */
trait QMAT
{
    /**
     * 返回除法的整数部分。 要放弃除法的余数时，可使用此函数
     *
     * @param double $numberator 必需。 被除数
     * @param double $denominator 必需。 除数
     *
     * @return integer
     */
    public static function quotient($numberator, $denominator)
    {
        return (int) ($numberator / $denominator);
    }
}
