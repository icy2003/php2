<?php
/**
 * Trait MathAndTrigonometry-O
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\icomponents\excel\mathAndTrigonometry;

/**
 * MathAndTrigonometry-O
 */
trait OMAT
{
    /**
     * 返回数字向上舍入到的最接近的奇数
     *
     * @param double $number 必需。 要舍入的值
     *
     * @return integer
     */
    public static function odd($number)
    {
        $sign = self::sign($number);
        $number = ceil(self::abs($number));
        if ($number % 2 == 0) {
            $number += 1;
        }
        return (int) ($sign * $number);
    }
}
