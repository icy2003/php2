<?php
/**
 * Trait MathAndTrigonometry-D
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\icomponents\excel\mathAndTrigonometry;

/**
 * MathAndTrigonometry-D
 */
trait DMAT
{
    /**
     * 按给定基数将数字的文本表示形式转换成十进制数
     *
     * @param string $text 必需
     * @param integer $radix 必需。Radix 必须是整数
     *
     * @return string
     */
    public static function decimal($text, $radix)
    {
        return base_convert($text, $radix, 10);
    }

    /**
     * 将弧度转换为度
     *
     * @param double $angle
     *
     * @return double
     */
    public static function degrees($angle)
    {
        return rad2deg($angle);
    }
}
