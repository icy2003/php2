<?php
/**
 * Trait MathAndTrigonometry-I
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\icomponents\excel\mathAndTrigonometry;

/**
 * MathAndTrigonometry-I
 */
trait IMAT
{
    /**
     * 将数字向下舍入到最接近的整数
     *
     * - int 是关键字，这里用上后缀 _i
     *
     * @param double $number 必需。 需要进行向下舍入取整的实数
     *
     * @return integer
     */
    public static function int_i($number)
    {
        return floor($number);
    }
}
