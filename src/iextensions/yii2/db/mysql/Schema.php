<?php

/**
 * Class Schema
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */

namespace t1h0\php\iextensions\yii2\db\mysql;

use yii\db\mysql\Schema as MysqlSchema;

/**
 * Schema 扩展
 */
class Schema extends MysqlSchema
{
    /**
     * mediumtext 类型
     */
    const TYPE_MEDIUMTEXT = 'mediumtext';
    /**
     * longtext 类型
     */
    const TYPE_LONGTEXT = 'longtext';
}
