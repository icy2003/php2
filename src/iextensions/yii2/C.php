<?php

namespace t1h0\php\iextensions\yii2;

use t1h0\php\C as PhpC;

class C extends PhpC
{
    const MYSQL_SERVER_HAS_GONE_AWAY = 2006;
    const MYSQL_LOST_CONNECTION_TO_MYSQL = 2013;
}
