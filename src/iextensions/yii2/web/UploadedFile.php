<?php
/**
 * Class UploadedFile
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */

namespace t1h0\php\iextensions\yii2\web;

use t1h0\php\iextensions\yii2\helpers\Html;
use yii\web\UploadedFile as U;

/**
 * UploadedFile 扩展
 */
class UploadedFile extends U
{
    /**
     * 获取单例
     *
     * @param yii\base\Model $model 模型对象
     * @param string $attribute
     * @param mixed $formName
     *
     * @return \yii\web\UploadedFile
     */
    public static function getInstance($model, $attribute, $formName = null)
    {
        $name = Html::getInputName($model, $attribute, $formName);
        return static::getInstanceByName($name);
    }
}
