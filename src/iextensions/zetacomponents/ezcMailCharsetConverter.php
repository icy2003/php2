<?php

namespace t1h0\php\iextensions\zetacomponents;

use ezcMailCharsetConverter as GlobalEzcMailCharsetConverter;
use t1h0\php\ihelpers\Charset;

class ezcMailCharsetConverter extends GlobalEzcMailCharsetConverter
{
    public static function convertToUTF8Iconv($text, $originalCharset)
    {
        return Charset::toUtf($text);
    }
}
