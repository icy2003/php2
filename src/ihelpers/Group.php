<?php

/**
 * Class Group
 *
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */

namespace t1h0\php\ihelpers;

use t1h0\php\I;

/**
 * 分组
 *
 * 如，安排考场。
 * ```php
 * $filePath = './31.2考试安排（公式）.xlsx';
 *
 * $records = ArrayStatic::toRecords(iterator_to_array(ExcelFile::rowGenerator([$filePath, '学生', "A1:K999"])));
 *
 * shuffle($records);
 *
 * $groupRecords = ArrayStatic::toRecords(iterator_to_array(ExcelFile::rowGenerator([$filePath, '学生', "M3:Q22"])));
 *
 * $group = new Group($groupRecords, $records, [
 *     // 规则一
 *     function (Group $current) {
 *         // 根据组合安排在对应考场，并且
 *         if ($current->getRecord('组合') == $current->getGroupRecord('组合')) {
 *             // 选择非日语考生
 *             if ($current->getRecord('是否日语') != '日语') {
 *                 // 不安排在17、18、19考场
 *                 if (false === in_array($current->getGroupRecord($current->getGroupKey()), [17, 18, 19])) {
 *                     return true;
 *                 }
 *             }
 *         }
 *     },
 *     // 规则二
 *     function (Group $current) {
 *         // 根据组合安排在对应考场，并且
 *         if ($current->getRecord('组合') == $current->getGroupRecord('组合')) {
 *             // 选择日语考生
 *             if ($current->getRecord('是否日语') == '日语') {
 *                 // 安排在17、18、19考场
 *                 if (in_array($current->getGroupRecord($current->getGroupKey()), [17, 18, 19])) {
 *                     return true;
 *                 }
 *             }
 *         }
 *     },
 * ], function (Group $current) {
 *     return [
 *         '考号' => '193'
 *         . ArrayStatic::matchByArray($current->getMember('方向'), ['物' => 2, '历' => 1])
 *         . str_pad($current->getMember('班级'), 2, '0', STR_PAD_LEFT)
 *         . str_pad($current->getMember($current->getGroupKey()), 2, '0', STR_PAD_LEFT)
 *         . str_pad($current->getMember($current->getMemberKey()), 2, '0', STR_PAD_LEFT),
 *     ];
 * }, function (Group $current) {
 *     // 剩下未安排的考生规则
 *     // 根据组合安排在对应考场，并且
 *     if ($current->getRecord('组合') == $current->getGroupRecord('组合')) {
 *         // 放在17、18、19考场
 *         if (in_array($current->getGroupRecord($current->getGroupKey()), [17, 18, 19])) {
 *             return true;
 *         }
 *     }
 * });
 * $group->setGroupKey('考场号')
 *     ->setGroupNameKey('考场位置')
 *     ->setMemberKey('座位号')
 *     ->setGroupMemberCountKey('人数');
 *
 * $members = $group->toRecords();
 *
 * ```
 */
class Group
{

    /**
     * 数据的记录数组
     * @var array
     */
    private $__records = [];

    /**
     * 分组的记录数组
     * @var array
     */
    private $__groupRecords = [];

    /**
     * 规则列表
     * @var callable[]
     */
    private $__ruleCallbacks = [];

    /**
     * 格式化数组回调（用于生成组和成员后）
     * @var callable
     */
    private $__formatCallback;

    /**
     * 剩余数据的分组规则
     * @var
     */
    private $__remainingCallback;

    public function __construct($groupRecords, $records, $ruleCallbacks, $formatCallback, $remainingCallback = null)
    {
        $this->__groupRecords = $groupRecords;
        $this->__records = $records;
        $this->__ruleCallbacks = $ruleCallbacks;
        $this->__formatCallback = $formatCallback;
        $this->__remainingCallback = $remainingCallback;
    }

    /**
     * 组编号
     * @var string
     */
    private $__groupKey = 'groupKey';

    /**
     * 设置组编号（组的表头），如考场号的数字（如10考场，10）
     * @param string $groupKey
     * @return static
     */
    public function setGroupKey($groupKey)
    {
        $this->__groupKey = $groupKey;

        return $this;
    }

    /**
     * 获取组编号（组的表头）
     * @return string
     */
    public function getGroupKey()
    {
        return $this->__groupKey;
    }

    /**
     * 组的名称的表头
     * @var string
     */
    private $__groupNameKey = 'groupNameKey';

    /**
     * 设置组的名称的表头，如考场的名称（如：三楼的第一间，301）
     * @param string $groupNameKey
     * @return static
     */
    public function setGroupNameKey($groupNameKey)
    {
        $this->__groupNameKey = $groupNameKey;

        return $this;
    }

    /**
     * 获取组的名称的表头
     * @return string
     */
    public function getGroupNameKey()
    {
        return $this->__groupNameKey;
    }

    /**
     * 成员人数的表头
     * @var string
     */
    private $__groupMemberCountKey = 'countKey';

    /**
     * 设置成员人数的表头
     * @param string $groupMemberCountKey
     * @return static
     */
    public function setGroupMemberCountKey($groupMemberCountKey)
    {
        $this->__groupMemberCountKey = $groupMemberCountKey;

        return $this;
    }

    /**
     * 成员编号的表头
     * @var string
     */
    private $__memberKey = 'memberKey';

    /**
     * 设置成员编号的表头
     * @param string $memberKey
     * @return static
     */
    public function setMemberKey($memberKey)
    {
        $this->__memberKey = $memberKey;

        return $this;
    }

    /**
     * 设置成员编号的表头
     * @return string
     */
    public function getMemberKey()
    {
        return $this->__memberKey;
    }

    /**
     * 数据记录
     * @var
     */
    private $__record;

    /**
     * 获取当前遍历的数据记录
     * @param string|null $key
     * @return mixed
     */
    public function getRecord($key = null)
    {
        return null === $key ? $this->__record : I::get($this->__record, $key);
    }

    /**
     * 组记录
     * @var
     */
    private $__groupRecord;

    /**
     * 获取当前遍历的组记录
     * @param string|null $key
     * @return mixed
     */
    public function getGroupRecord($key = null)
    {
        return null === $key ? $this->__groupRecord : I::get($this->__groupRecord, $key);
    }

    /**
     * 成员
     * @var
     */
    private $__member;

    /**
     * 获取当前遍历的成员
     * @param string|null $key
     * @return mixed
     */
    public function getMember($key = null)
    {
        return null === $key ? $this->__member : I::get($this->__member, $key);
    }

    /**
     * 成员列表
     * @var array
     */
    private $__members = [];

    /**
     * 生成最后的分组
     * @return array
     */
    public function toRecords()
    {
        $groupCount = array_fill(1, count($this->__groupRecords), 0);
        foreach ($this->__ruleCallbacks as $ruleCallback) {
            foreach ($this->__records as $key => $record) {
                $this->__record = $record;
                $this->__groupMember($key, $record, $groupCount, $ruleCallback);
            }
        }
        $this->__records = $this->__members;
        if ($this->__remainingCallback) {
            foreach ($this->__records as $key => $record) {
                if (null === $record[$this->__groupKey]) {
                    $this->__record = $record;
                    $this->__groupMember($key, $record, $groupCount, $this->__remainingCallback);
                }
            }
        }
        return $this->__members;
    }

    /**
     * @ignore
     */
    private function __groupMember($key, $record, &$groupCount, $callback)
    {
        if (null === $this->__members[$key]) {
            $this->__members[$key] = $record;
            unset($this->__members[$key][$this->__groupNameKey]);
            unset($this->__members[$key][$this->__groupKey]);
            unset($this->__members[$key][$this->__memberKey]);
        }
        $this->__member = $this->__members[$key];
        foreach ($this->__groupRecords as $groupRecord) {
            $this->__groupRecord = $groupRecord;
            if ($groupRecord[$this->__groupMemberCountKey] && $groupRecord[$this->__groupMemberCountKey] > 0) {
                if (I::call($callback, [$this])) {
                    if ($groupCount[$groupRecord[$this->__groupKey]] < $groupRecord[$this->__groupMemberCountKey]) {
                        $groupCount[$groupRecord[$this->__groupKey]]++;
                        $this->__members[$key][$this->__groupNameKey] = $groupRecord[$this->__groupNameKey];
                        $this->__members[$key][$this->__groupKey] = $groupRecord[$this->__groupKey];
                        $this->__members[$key][$this->__memberKey] = $groupCount[$groupRecord[$this->__groupKey]];
                        $this->__member = $this->__members[$key];
                        $this->__members[$key] = ArrayStatic::merge($this->__members[$key], I::call($this->__formatCallback, [$this]));
                        break;
                    } else {
                        continue;
                    }
                }
            }
        }
    }
}
