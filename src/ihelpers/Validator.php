<?php

/**
 * Class Validator
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */
namespace t1h0\php\ihelpers;

use t1h0\php\I;

/**
 * 验证器.
 */
class Validator
{
    private $__data = [];
    private $__old_data = [];
    private $__safeField = [];
    private $__messages = [];
    private $__codes = [];

    /**
     * @var self 验证成功
     */
    const CODE_SUCCEEDED = 0;
    /**
     * @var self 验证失败
     */
    const CODE_VALIDATE_FAILED = -1;

    /**
     * @var self 必填验证失败
     */
    const CODE_VALIDATE_REQUIRED = -2;
    /**
     * @var self 范围验证失败
     */
    const CODE_VALIDATE_IN = -3;
    /**
     * @var self 正则验证失败
     */
    const CODE_VALIDATE_MATCH = -4;
    /**
     * @var self 手机号格式验证失败
     */
    const CODE_VALIDATE_MOBILE = -5;
    /**
     * @var self 邮箱格式验证失败
     */
    const CODE_VALIDATE_EMAIL = -6;
    /**
     * @var self 唯一性验证失败
     */
    const CODE_VALIDATE_UNIQUE = -7;
    /**
     * @var self 回调验证失败
     */
    const CODE_VALIDATE_CALL = -8;

    /**
     * @var self 必填验证器
     */
    const VALIDATOR_REQUIRED = '_required';
    /**
     * @var self 范围验证器
     */
    const VALIDATOR_IN = '_in';
    /**
     * @var self 正则验证器
     */
    const VALIDATOR_MATCH = '_match';
    /**
     * @var self 手机号格式验证器
     */
    const VALIDATOR_MOBILE = '_mobile';
    /**
     * @var self 邮箱格式验证器
     */
    const VALIDATOR_EMAIL = '_email';
    /**
     * @var self 唯一性验证器
     */
    const VALIDATOR_UNIQUE = '_unique';
    /**
     * @var self 回调验证器
     */
    const VALIDATOR_CALL = '_call';

    /**
     * @var self 默认值过滤器
     */
    const FILTER_DEFAULT = '_default';
    /**
     * @var self 设置过滤器
     */
    const FILTER_SET = '_set';
    /**
     * @var self 回调过滤器
     */
    const FILTER_FILTER = '_filter';
    /**
     * @var self 安全过滤器
     */
    const FILTER_SAFE = '_safe';
    /**
     * @var self 删除过滤器
     */
    const FILTER_UNSET = '_unset';

    /**
     * 预加载数据
     *
     * @param array $data
     * @return static
     */
    public function load($data)
    {
        $this->__data = $data;
        $this->__old_data = $data;
        return $this;
    }

    /**
     * 验证规则
     *
     * @param array $rules
     * @return static
     */
    public function rules($rules)
    {
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                if (false === ArrayStatic::keyExistsAll([0, 1], $rule)) {
                    throw new \Exception('rules error');
                }
                $fieldArray = Strings::toArray($rule[0]);
                $ruleName = $rule[1];
                $method = $ruleName . 'Validator';
                if (method_exists($this, $method)) {
                    foreach ($fieldArray as $field) {
                        if ($this->$method($this->__old_data, $field, $rule)) {
                            array_push($this->__safeField, $field);
                        }
                    }
                } else {
                    echo $method;
                    throw new \Exception('method error');
                }
            }
        }
        return $this;
    }

    protected function _requiredValidator($data, $field, $rule)
    {
        if (null === I::get($data, $field)) {
            $this->__messages[$field][] = I::get($rule, 'message', $field . ' 必填');
            $this->__codes[$field][] = I::get($rule, 'code', self::CODE_VALIDATE_REQUIRED);
            return false;
        }
        return true;
    }

    protected function _inValidator($data, $field, $rule)
    {
        if (!array_key_exists('range', $rule)) {
            throw new \Exception('range error');
        }
        $value = I::get($data, $field);
        $range = (array) I::get($rule, 'range', []);
        $isStrict = (bool) I::get($rule, 'isStrict', false);
        if (!in_array($value, $range, $isStrict)) {
            $this->__messages[$field][] = I::get($rule, 'message', $field . ' 不在范围内');
            $this->__codes[$field][] = I::get($rule, 'code', self::CODE_VALIDATE_IN);
            return false;
        }
        return true;
    }

    protected function _matchValidator($data, $field, $rule)
    {
        if (!array_key_exists('pattern', $rule)) {
            throw new \Exception('pattern error');
        }
        $value = (string) I::get($data, $field);
        $pattern = (string) I::get($rule, 'pattern', '//');
        if (!preg_match($pattern, $value)) {
            $this->__messages[$field][] = I::get($rule, 'message', $field . ' 格式不正确');
            $this->__codes[$field][] = I::get($rule, 'code', self::CODE_VALIDATE_MATCH);
            return false;
        }
        return true;
    }

    protected function _mobileValidator($data, $field, $rule)
    {
        $rule['pattern'] = '/^1\\d{10}$/';
        $rule['message'] = I::get($rule, 'message', $field . ' 手机号格式不正确');
        $rule['code'] = I::get($rule, 'code', self::CODE_VALIDATE_MOBILE);
        return $this->_matchValidator($data, $field, $rule);
    }

    protected function _emailValidator($data, $field, $rule)
    {
        $rule['pattern'] = '/^[\w\-\.]+@[\w\-]+(\.\w+)+$/';
        $rule['message'] = I::get($rule, 'message', $field . ' 邮箱格式不正确');
        $rule['code'] = I::get($rule, 'code', self::CODE_VALIDATE_EMAIL);
        return $this->_matchValidator($data, $field, $rule);
    }

    protected function _uniqueValidator($data, $field, $rule)
    {
        $value = I::get($data, $field);
        if (array_key_exists('model', $rule)) {

        } else {
            $function = I::get($rule, 'function');
            if (!is_callable($function)) {
                throw new \Exception('function error');
                return false;
            }
            if (!$function($value)) {
                $this->__messages[$field][] = I::get($rule, 'message', $field . ' 不唯一');
                $this->__codes[$field][] = I::get($rule, 'code', self::CODE_VALIDATE_UNIQUE);
                return false;
            }
        }
        return true;
    }

    protected function _callValidator($data, $field, $rule)
    {
        if (!array_key_exists('function', $rule)) {
            throw new \Exception('function error');
            return false;
        }
        $function = I::get($rule, 'function');
        if (!is_callable($function)) {
            throw new \Exception('function call error');
            return false;
        }
        if (!$function(I::get($data, $field))) {
            $this->__messages[$field][] = I::get($rule, 'message', $field . ' 验证不通过');
            $this->__codes[$field][] = I::get($rule, 'code', self::CODE_VALIDATE_CALL);
            return false;
        }
        return true;
    }

    protected function _defaultValidator($data, $field, $rule)
    {
        if (!array_key_exists('value', $rule)) {
            throw new \Exception('value error');
            return false;
        }
        $value = I::get($rule, 'value');
        $isStrict = I::get($rule, 'isStrict', false);
        $defaultValue = is_callable($value) ? $value() : $value;
        if (true === $isStrict) {
            $this->__data[$field] = I::get($data, $field, $defaultValue);
        } else {
            $this->__data[$field] = !empty($data[$field]) ? $data[$field] : $defaultValue;
        }
        return true;
    }

    protected function _setValidator($data, $field, $rule)
    {
        if (!array_key_exists('value', $rule)) {
            throw new \Exception('value error');
            return false;
        }
        $value = I::get($rule, 'value');
        $this->__data[$field] = is_callable($value) ? $value() : $value;
        return true;
    }

    protected function _filterValidator($data, $field, $rule)
    {
        if (!array_key_exists('function', $rule)) {
            throw new \Exception('function error');
            return false;
        }
        $function = I::get($rule, 'function');
        if (!is_callable($function)) {
            throw new \Exception('function call error');
            return false;
        }
        $this->__data[$field] = $function(I::get($data, $field));
        return true;
    }

    protected function _safeValidator($data, $field, $rule)
    {
        return true;
    }

    protected function _unsetValidator($data, $field, $rule)
    {
        unset($this->__data[$field]);
        return true;
    }

    public function getMessages()
    {
        return $this->__messages;
    }

    public function getMessage()
    {
        foreach ($this->__messages as $field => $messages) {
            foreach ($messages as $k => $message) {
                $code = $this->__codes[$field][$k];
                return [$code, $message];
            }
        }
        return [self::CODE_SUCCEEDED, 'success'];
    }

    public function getData()
    {
        return ArrayStatic::some($this->__data, $this->__safeField);
    }
}
