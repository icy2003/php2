<?php
/**
 * Class LocalFile
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */
namespace t1h0\php\ihelpers\file;

use t1h0\php\I;

/**
 * 本地文件方法类.
 */
class LocalFile
{

    /**
     * 初始化配置
     *
     * @var array{useCache?:boolean,bufferSize?:integer,locale?:string}
     */
    protected $_config = [];

    /**
     * 单例对象
     *
     * @var static
     */
    protected static $_instance;

    /**
     * 构造函数
     */
    protected function __construct()
    {
    }

    /**
     * 克隆函数
     *
     * @return void
     */
    protected function __clone()
    {
    }

    /**
     * 创建文件对象的单例.
     *
     * @param array $config
     *  - useCache: 对于一些函数，php会把执行过的结果缓存起来加快执行速度，如果该值为 true，则后续结果可能会使用缓存，默认不使用缓存 https://www.php.net/manual/zh/function.clearstatcache.php
     *  - bufferSize: 缓冲区大小，默认 4096
     *  - locale: 地区信息，默认 'zh_CN.UTF-8'
     *
     * @return static
     */
    public static function instance($config = [])
    {
        if (!static::$_instance instanceof static ) {
            static::$_instance = new static();
        }
        static::$_instance->_config = array_merge(static::$_instance->_config, [
            'useCache' => I::get($config, 'useCache', false),
            'bufferSize' => I::get($config, 'bufferSize', 4096),
            'locale' => I::get($config, 'locale', 'zh_CN.UTF-8'),
        ]);

        return static::$_instance;
    }

    /**
     * 改变文件（或递归目录）的组.
     * - chgrp 的递归版本
     * - 此函数在 Windows 上会静默失败
     * - @link https://www.php.net/manual/zh/function.chgrp.php
     *
     * @param string $fdpath 文件（或目录）的路径（或别名）
     * @param integer|string $group 组名或数字
     * @return boolean
     */
    public function changeGroupRecursive($fdpath, $group)
    {
        $fdpathValue = $this->getPathValue($fdpath);
        return $this->__changeRecursive('chgrp', $fdpathValue, [$group]);
    }

    /**
     * 改变文件（或递归目录）的文件模式.
     * - chmod 的递归版本
     * - Windows 上权限不同，表现不一致
     * - @link https://www.php.net/manual/zh/function.chmod.php
     *
     * @param string $fdpath 文件（或目录）的路径（或别名）
     * @param integer $mode 文件模式
     * @return boolean
     */
    public function changeModeRecursive($fdpath, $mode)
    {
        $fdpathValue = $this->getPathValue($fdpath);
        return $this->__changeRecursive('chmod', $fdpathValue, [$mode]);
    }

    /**
     * 改变文件（或递归目录）的所有者.
     * - chown 的递归版本
     * - 此函数在 Windows 上会静默失败
     * - @link https://www.php.net/manual/zh/function.chown.php
     *
     * @param string $fdpath 文件（或目录）的路径（或别名）
     * @param integer|string $user 用户名或数字
     * @return boolean
     */
    public function changeOwnRecursive($fdpath, $user)
    {
        $fdpathValue = $this->getPathValue($fdpath);
        return $this->__changeRecursive('chown', $fdpathValue, [$user]);
    }

    /**
     * 递归地改变文件和目录的属性
     *
     * @param callable $callback 改变的递归回调函数，目前支持：chmod、chgrp、chown
     * @param string $fdpath 文件或目录的路径
     * @param array $args 额外的参数
     * @return boolean
     */
    private function __changeRecursive($callback, $fdpath, $args)
    {
        // 检查路径是否存在
        if (!file_exists($fdpath)) {
            return false;
        }

        if (!call_user_func_array($callback, array_merge([$fdpath], $args))) {
            return false;
        }

        // 如果是目录，则递归修改子目录和文件的权限
        if (is_dir($fdpath)) {
            $dir = new \DirectoryIterator($fdpath);

            foreach ($dir as $entry) {
                // 跳过目录中的当前目录（.）和上级目录（..）
                if ($entry->isDot()) {
                    continue;
                }

                $subPath = $entry->getPathname();
                if (!$entry->isDir()) {
                    // 如果是文件，直接修改权限
                    if (!call_user_func_array($callback, array_merge([$subPath], $args))) {
                        return false;
                    }
                } else {
                    // 如果是目录，递归修改权限
                    if (!$this->__changeRecursive($callback, $subPath, $args)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * 清除文件状态缓存
     *
     * @return void
     */
    public function clearCache()
    {
        true === $this->_config['useCache'] || clearstatcache();
    }

    /**
     * 关闭文件对象接口.
     *
     * @param \SplFileObject $file 文件对象接口
     * @return true
     */
    public function close($file)
    {
        // 关闭文件对象（SplFileObject在析构时会自动关闭）
        $file = null;
        unset($file);
        return true;
    }

    /**
     * 将文件以二进制的方式读取一段（或全部）
     *
     * @param string $filePath 要读取的文件的路径（或别名）
     * @param integer $startByte 开始字节位置
     * @param integer|null $byteLength 读取字节长度
     *
     * @return string|false
     */
    public function contentReadAsData($filePath, $startByte = 0, $byteLength = null)
    {
        // 打开文件
        $filePathValue = $this->getPathValue($filePath);
        $file = fopen($filePathValue, 'rb');
        if (false === $file) {
            return false;
        }

        // 跳转到指定字节起点
        if (false === fseek($file, $startByte, SEEK_SET)) {
            return false;
        }
        $data = '';
        // 读取指定长度的字节
        if (null === $byteLength) {
            // 如果长度未指定，读取从当前位置到文件末尾的所有内容
            while (false === feof($file)) {
                $data .= fread($file, $this->_config['bufferSize']); // 读取8192字节，你可以根据需要调整这个值
            }
        } else {
            $data = fread($file, $byteLength);
        }

        // 关闭文件
        fclose($file);

        return $data;
    }

    /**
     * 将文件以文本的方式读取一段（或全部）
     *
     * @param string $filePath 要读取的文件的路径（或别名）
     * @param integer $startChar 开始字符位置
     * @param integer|null $charLength 读取字符长度
     * @return string|false
     */
    public function contentReadAsText($filePath, $startChar = 0, $charLength = null)
    {
        $segment = '';
        $currentCharPosition = 0;
        $bytesRead = 0;
        $multiByteCharLength = 0;

        // 创建 SplFileObject 实例
        $file = new \SplFileObject($filePath, 'r'); // 'r' 表示只读模式，不锁定文件

        // 逐字符读取，直到达到指定的字符位置
        while (!$file->eof()) {
            if ($startChar > 0 && $currentCharPosition < $startChar) {
                $char = $file->fgetc(); // 读取一个字符
                $charLength = mb_strlen($char, 'UTF-8'); // 获取字符长度

                // 如果当前字符是多字节字符的一部分，则继续读取
                while ($charLength == 0 && !$file->eof()) {
                    $char .= $file->fgetc(); // 读取下一个字符
                    $charLength = mb_strlen($char, 'UTF-8'); // 重新计算字符长度
                }

                // 如果读取到了多字节字符的开始，则增加其长度
                if ($charLength > 1) {
                    $multiByteCharLength = $charLength;
                }

                // 更新当前字符位置和已读取的字节数
                $currentCharPosition++;
                $bytesRead += strlen($char);

                // 如果当前字符是多字节字符的一部分，则继续读取剩余的部分
                if ($multiByteCharLength > 1) {
                    $file->fseek($multiByteCharLength - 1, SEEK_CUR); // 移动文件指针到多字节字符的末尾
                    $multiByteCharLength = 0; // 重置多字节字符长度
                }
            } else {
                break;
            }
        }

        // 现在我们已经定位到了指定的字符位置，开始读取指定长度的字符段
        while (!$file->eof()) {
            if (null === $charLength || mb_strlen($segment, 'UTF-8') < $charLength) {
                $char = $file->fgetc();
                $segment .= $char;
                $bytesRead++;
            } else {
                break;
            }
        }
        $this->close($file);

        // 返回读取到的字符段
        return $segment;
    }

    /**
     * 将数据写入文件.
     * - 该函数等同于 `file_put_contents`，但在处理大数据量（即大文件）时进行了优化
     * - @link https://www.php.net/manual/zh/function.file-put-contents.php
     *
     * @param string $filePath 要被写入数据的文件的路径（或别名）
     * @param mixed $data 要写入的数据
     * @param integer $flags 可以是 以下 flag 使用 OR (|) 运算符进行的组合：
     *      - FILE_USE_INCLUDE_PATH：在 include 目录里搜索 filename
     *      - FILE_APPEND：如果文件 filename 已经存在，追加数据而不是覆盖
     *      - LOCK_EX：在写入时获取文件独占锁
     * @param resource $context 一个 context 资源
     * @return integer|false 该函数将返回写入到文件内数据的字节数，失败时返回false
     */
    public function contentWrite($filePath, $data, $flags = 0, $context = null)
    {

        // 创建文件句柄
        $handle = @fopen($this->getPathValue($filePath), ($flags & FILE_APPEND) ? 'ab' : 'wb', ($flags & FILE_USE_INCLUDE_PATH) ? FILE_USE_INCLUDE_PATH : false, $context ? stream_context_create(stream_context_get_options($context)) : null);
        if (false === $handle) {
            return false;
        }

        // 锁定文件（如果指定了 LOCK_EX 标志）
        if ($flags & LOCK_EX) {
            flock($handle, LOCK_EX);
        }

        // 分块写入数据
        $chunkSize = I::get($this->_config, 'bufferSize', 4096); // 设置块大小
        $length = strlen($data);
        $position = 0;
        while ($position < $length) {
            $chunk = substr($data, $position, $chunkSize);
            $bytesWritten = fwrite($handle, $chunk);

            // 检查是否写入成功
            if ($bytesWritten === false) {
                // 解锁文件（如果之前锁定了）
                if ($flags & LOCK_EX) {
                    flock($handle, LOCK_UN);
                }
                fclose($handle);
                return false;
            }

            $position += $bytesWritten;
        }

        // 解锁文件（如果之前锁定了）
        if ($flags & LOCK_EX) {
            flock($handle, LOCK_UN);
        }

        // 清理缓冲区并关闭文件句柄
        fflush($handle);
        fclose($handle);

        return $position; // 返回写入的字节数
    }

    /**
     * 复制目录为（另一个目录）.
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromDirPath 原始目录的路径（或别名）
     * @param string $asDirPath 目标目录的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function copyDirAs($fromDirPath, $asDirPath, $isOverwrite = true)
    {
        $fromDirPathValue = $this->getPathValue($fromDirPath);
        $asDirPathValue = $this->getPathValue($asDirPath);
        if (false === $this->isDir($fromDirPathValue)) {
            return false;
        }
        if (true === $this->isDir($asDirPathValue) && false === $isOverwrite) {
            return false;
        }
        if (false === $this->isDir($asDirPathValue)) {
            $this->createDir($asDirPathValue, 0777);
        }
        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($fromDirPathValue, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iterator as $entry) {
            /**
             * @var \SplFileInfo $entry
             */
            /**
             * @var \RecursiveDirectoryIterator $subIterator
             */
            $subIterator = $iterator->getSubIterator();
            $targetPathValue = $this->getPathValue($asDirPathValue . DIRECTORY_SEPARATOR . $subIterator->getSubPathname());
            if ($entry->isDir()) {
                if (false === $this->isDir($targetPathValue)) {
                    $this->createDir($targetPathValue, 0777);
                }
            } else {
                $this->copyDirAs($entry->getPathname(), $targetPathValue, $isOverwrite);
            }
        }
        return true;
    }

    /**
     * 复制目录到（某个目录）
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromDirPath 原始目录的路径（或别名）
     * @param string $toDirPath 目标目录的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function copyDirTo($fromDirPath, $toDirPath, $isOverwrite = true)
    {
        $toDirPath = $toDirPath . DIRECTORY_SEPARATOR . basename($fromDirPath);
        return $this->copyDirAs($fromDirPath, $toDirPath, $isOverwrite);
    }

    /**
     * 复制文件为（另一个文件）
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromFilePath 原始文件的路径（或别名）
     * @param string $asFilePath 目标文件的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function copyFileAs($fromFilePath, $asFilePath, $isOverwrite = true)
    {
        $asFilePathValue = $this->getPathValue($asFilePath);
        if ($this->isFile($asFilePathValue) && false === $isOverwrite) {
            return false;
        }
        $fromFilePathValue = $this->getPathValue($fromFilePath);
        $toDirPathValue = dirname($asFilePathValue);
        if (false === $this->isDir($toDirPathValue)) {
            $this->createDir($toDirPathValue, 0777);
        }
        return @copy($fromFilePathValue, $asFilePathValue);
    }

    /**
     * 复制文件到（某个目录）
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromFilePath 原始文件的路径（或别名）
     * @param string $toDirPath 目标目录的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function copyFileTo($fromFilePath, $toDirPath, $isOverwrite = true)
    {
        $toDirPath = $toDirPath . DIRECTORY_SEPARATOR . basename($fromFilePath);
        return $this->copyFileAs($fromFilePath, $toDirPath, $isOverwrite);
    }

    /**
     * 递归地创建目录
     *
     * @param string $dirPath 目录的路径（或别名）
     * @param integer $mode 文件模式
     * @return boolean
     */
    public function createDir($dirPath, $mode = 0777)
    {
        $dirPathValue = $this->getPathValue($dirPath);
        if (is_dir($dirPathValue)) {
            // 目录已存在，无需创建
            return true;
        }

        // 递归创建父目录
        if (!is_dir(dirname($dirPathValue))) {
            $this->createDir(dirname($dirPathValue), $mode);
        }

        // 创建目录
        return mkdir($dirPathValue, $mode, true);
    }

    /**
     * 生成器：读取为 csv 数组
     * - 大文件进行了优化处理
     * - @link https://www.php.net/manual/zh/function.fgetcsv.php
     *
     * @param string $filePath 文件的路径（或别名）
     * @param string $delimiter 设置字段分隔符（仅一个单字节字符）
     * @param string $enclosure 设置字段环绕符（仅一个单字节字符）
     * @param string $escape 设置转义字符（最多一个单字节字符）。空字符串（""）禁用所有的转义机制
     * @return \Generator
     */
    public function csvGenerator($filePath, $delimiter = ',', $enclosure = '"', $escape = '\\')
    {
        $filePathValue = $this->getPathValue($filePath);
        try {
            if (false !== ($file = new \SplFileObject($filePathValue, 'r'))) {
                while (($data = $file->fgetcsv($delimiter, $enclosure, $escape)) !== false) {
                    yield $data;
                }
            }
        } finally {
            $this->close($file);
        }
    }

    /**
     * csv 转成数据库记录格式：key=>value
     *
     * @param string $filePath 文件的路径（或别名）
     * @param boolean $skipEmptyRow 是否跳过空行，默认 true，跳过
     * @return array
     */
    public function csvToRecords($filePath, $skipEmptyRow = true)
    {
        $fields = [];
        $records = [];
        foreach ($this->csvGenerator($filePath) as $rowIndex => $array) {
            if (0 == $rowIndex) {
                $fields = $array;
            } else {
                if (true === $skipEmptyRow && [null] === $array) {
                    continue;
                }
                foreach ($array as $columnIndex => $cell) {
                    $records[$rowIndex - 1][$fields[$columnIndex]] = $cell;
                }
            }
        }
        return $records;
    }

    /**
     * 生成器：按字节长读取文件
     *
     * @param string $filePath 文件的路径
     *
     * @return \Generator
     */
    public function dataGenerator($filePath)
    {
        $bufferSizeTotal = 0;
        $bufferSize = I::get($this->_config, 'bufferSize', 4096);
        $file = new \SplFileObject($filePath, 'r');
        $size = $file->getSize();
        try {
            while (!$file->eof() && $size > $bufferSizeTotal) {
                $bufferSizeTotal += $bufferSize;
                yield $file->fread($bufferSizeTotal);
            }
        } finally {
            $this->close($file);
        }
    }

    /**
     * 删除一个目录
     * - 目录下的文件和目录会被递归地删除
     *
     * @param string $dirPath 目录的路径（或别名）
     *
     * @return boolean
     */
    public function deleteDir($dirPath)
    {
        $dirPathValue = $this->getPathValue($dirPath);
        if (true === $this->isFile($dirPathValue)) {
            return false;
        }
        if (false === $this->isDir($dirPathValue)) {
            return true;
        }
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dirPathValue, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST) as $entry) {
            /**
             * @var \SplFileInfo $entry
             */
            $entry->isFile() ? unlink($entry->getPathname()) : rmdir($entry->getPathname());
        }
        return rmdir($dirPathValue);
    }

    /**
     * 删除一个文件
     *
     * @param string $filePath 文件的路径（或别名）
     *
     * @return boolean
     */
    public function deleteFile($filePath)
    {
        $filePathValue = $this->getPathValue($filePath);
        if ($this->isFile($filePathValue)) {
            return @unlink($filePathValue);
        }
        return true;
    }

    /**
     * 生成器：读取为按分隔符切割成的数组
     * - 分隔符可以是任意长
     * - 大文件进行了优化处理
     *
     * @param string $filePath 文件的路径（或别名）
     * @param string $delimiter 分隔符，默认 PHP_EOL
     * @return \Generator
     */
    public function delimiterGenerator($filePath, $delimiter = PHP_EOL)
    {
        $filePathValue = $this->getPathValue($filePath);
        $file = new \SplFileObject($filePathValue, 'r');
        try {
            $bufferSize = $this->_config['bufferSize']; // 可以根据文件大小和内存限制调整缓冲区大小
            $buffer = '';
            $delimiterLength = strlen($delimiter);

            while (false === $file->eof()) {
                $buffer .= $file->fread($bufferSize);

                // 查找分隔符在缓冲区中的位置
                $pos = strpos($buffer, $delimiter);

                // 如果找到了分隔符
                while (false !== $pos) {
                    // 提取分隔符之前的字符串
                    $item = substr($buffer, 0, $pos);
                    yield $item;

                    // 移除已经处理的部分
                    $buffer = substr($buffer, $pos + $delimiterLength);

                    // 继续查找剩余的缓冲区内容
                    $pos = strpos($buffer, $delimiter);
                }
            }

            // 处理文件末尾可能剩下的内容
            if (!empty($buffer)) {
                yield $buffer;
            }
        } finally {
            $this->close($file);
        }
    }

    /**
     * 取得文件的上次访问时间
     *
     * @param string $filePath 文件的路径（或别名）
     * @return integer|false
     */
    public function getAccessTime($filePath)
    {
        $this->clearCache();
        return fileatime($this->getPathValue($filePath));
    }

    /**
     * 返回路径中的文件名部分.
     * - getBasename("/etc/sudoers.d", ".d")=>"sudoers"
     * - getBasename("/etc/sudoers.d")=>"sudoers.d"
     * - getBasename("/etc/passwd")=>"passwd"
     * - getBasename("/etc/")=>"etc"
     * - getBasename(".")=>"."
     * - getBasename("/")=>""
     *
     * @param string $fdpath 文件（或目录）的路径（或别名），会将 `\` 转成 `/`。
     * @param string $suffix 如果文件名是以 `$suffix` 结束的，那这一部分也会被去掉。
     *
     * @return string
     */
    public function getBaseName($fdpath, $suffix = null)
    {
        return basename($this->getPathValue($fdpath), $suffix);
    }

    /**
     * 取得文件的 inode 修改时间，该时间可能是：
     * - 文件创建时间：如果需要实际创建时间，需要自己实现
     * - 文件元数据（如权限、所有者）被更改时间
     *
     * @param string $filePath 文件的路径（或别名）
     * @return integer|false
     */
    public function getCreateTime($filePath)
    {
        $this->clearCache();
        return filectime($this->getPathValue($filePath));
    }

    /**
     * 返回路径中的目录部分
     *
     * @param string $fdpath 文件（或目录）的路径（或别名）。会将 `\` 转成 `/`。
     * @param integer $levelCount 要向上的父目录数量。整型，必须大于 0
     *
     * @return string
     */
    public function getDirName($fdpath, $levelCount = 1)
    {
        return dirname($this->getPathValue($fdpath), $levelCount);
    }

    /**
     * 返回目录中的可用空间
     *
     * @param string $dirPath 文件系统目录或者磁盘分区的路径（或别名）
     * @return float|false 以浮点返回可用的字节数， 或者在失败时返回 false
     */
    public function getDiskFreeSpace($dirPath)
    {
        $dirPathValue = $this->getPathValue($dirPath);
        return disk_free_space($dirPathValue);
    }

    /**
     * 返回一个目录的磁盘总大小
     *
     * @param string $dirPath 文件系统的目录或者磁盘分区的路径（或别名）
     * @return float|false 以浮点返回一个目录的磁盘总大小字节数， 或者在失败时返回 false
     */
    public function getDiskTotalSpace($dirPath)
    {
        $dirPathValue = $this->getPathValue($dirPath);
        return disk_total_space($dirPathValue);
    }

    /**
     * 获取文件扩展名
     * - 不带点（.）
     *
     * @param string $filePath 文件的路径（或别名）
     * @return string
     */
    public function getExtension($filePath)
    {
        setlocale(LC_ALL, $this->_config['locale']);
        return strtolower(pathinfo($this->getPathValue($filePath), PATHINFO_EXTENSION));
    }

    /**
     * 获取文件名
     * - 不带扩展名和路径的名字
     *
     * @param string $filePath 文件的路径（或别名）
     * @return string
     */
    public function getFileName($filePath)
    {
        setlocale(LC_ALL, $this->_config['locale']);
        return pathinfo($this->getPathValue($filePath), PATHINFO_FILENAME);
    }

    /**
     * 取得文件的组 ID
     * - root 用户返回 0
     * - Windows 下永远都返回 0
     *
     * @param string $filePath 文件的路径（或别名）
     * @return integer|false
     */
    public function getGroupId($filePath)
    {
        $this->clearCache();
        return filegroup($this->getPathValue($filePath));
    }

    /**
     * 取得文件的组信息
     *
     * - 返回格式：
     * ```
     *  [
     *      [name]    => toons
     *      [passwd]  => x
     *      [members] => [
     *                      [0] => tom
     *                      [1] => jerry
     *              ]
     *      [gid]     => 42
     *  ]
     * ```
     *
     * @param string $filePath 文件的路径（或别名）
     * @return array|false 文件的组信息数组
     */
    public function getGroupInfo($filePath)
    {
        $this->clearCache();
        $filePathValue = $this->getPathValue($filePath);
        $groupId = filegroup($filePathValue);
        if (false === $groupId) {
            return false;
        }
        if (true === extension_loaded('posix')) {
            return posix_getgrgid($groupId);
        }
        return false;
    }

    /**
     * 取得文件修改时间
     *
     * @param string $filePath 文件的路径（或别名）
     * @return integer|false
     */
    public function getModifyTime($filePath)
    {
        $this->clearCache();
        return filemtime($this->getPathValue($filePath));
    }

    /**
     * 取得文件的所有者 ID
     * - root 用户返回 0
     * - Windows 下永远都返回 0
     *
     * @param string $filePath 文件的路径（或别名）
     * @return integer|false
     */
    public function getOwnerId($filePath)
    {
        $this->clearCache();
        return fileowner($this->getPathValue($filePath));
    }

    /**
     * 取得文件的所有者信息
     *
     * - 返回格式：
     * ```
     *  [
     *      [name]    => tom
     *      [passwd]  => x
     *      [uid]     => 10000
     *      [gid]     => 42
     *      [gecos]   => "tom,,,"
     *      [dir]     => "/home/tom"
     *      [shell]   => "/bin/bash"
     *  ]
     * ```
     *
     * @param string $filePath 文件的路径（或别名）
     * @return array|false 文件的所有者信息数组
     */
    public function getOwnerInfo($filePath)
    {
        $this->clearCache();
        $filePathValue = $this->getPathValue($filePath);
        $ownerId = fileowner($filePathValue);
        if (false === $ownerId) {
            return false;
        }
        if (true === extension_loaded('posix')) {
            return posix_getgrgid($ownerId);
        }
        return false;
    }

    /**
     * 根据文件（或目录）的路径（或别名）获取真实路径
     *
     * @param string $fdpath
     * @return string
     */
    public function getPathValue($fdpath)
    {
        return rtrim(I::getAlias(str_replace('\\', '/', $fdpath)), '/');
    }

    /**
     * 获取文件的数字权限
     *
     * @param string $fdPath 文件（或目录）的路径（或别名）
     * @return integer|false 以八进制的形式显示文件的权限
     */
    public function getPermissionAsString($fdPath)
    {
        $this->clearCache();
        $fdPathValue = $this->getPathValue($fdPath);
        $perms = fileperms($fdPathValue);
        // 所有者
        $charPerms = (($perms & 0x0100) ? 'r' : '-');
        $charPerms .= (($perms & 0x0080) ? 'w' : '-');
        $charPerms .= (($perms & 0x0040) ?
            (($perms & 0x0800) ? 's' : 'x') :
            (($perms & 0x0800) ? 'S' : '-'));

        // 组
        $charPerms .= (($perms & 0x0020) ? 'r' : '-');
        $charPerms .= (($perms & 0x0010) ? 'w' : '-');
        $charPerms .= (($perms & 0x0008) ?
            (($perms & 0x0400) ? 's' : 'x') :
            (($perms & 0x0400) ? 'S' : '-'));

        // 其它
        $charPerms .= (($perms & 0x0004) ? 'r' : '-');
        $charPerms .= (($perms & 0x0002) ? 'w' : '-');
        $charPerms .= (($perms & 0x0001) ?
            (($perms & 0x0200) ? 't' : 'x') :
            (($perms & 0x0200) ? 'T' : '-'));

        return $charPerms;
    }

    /**
     * 获取文件的字符权限
     *
     * @param string $fdPath 文件（或目录）的路径（或别名）
     * @return integer|false 以字符的形式显示文件的权限
     */
    public function getPermissionAsNumber($fdPath)
    {
        $this->clearCache();
        $fdPathValue = $this->getPathValue($fdPath);
        return sprintf('%03o', fileperms($fdPathValue) & 0777);
    }

    /**
     * 获取权限中的文件类型
     *
     * @param string $fdPath 文件（或目录）的路径（或别名）
     * @return string
     */
    public function getPermissionType($fdPath)
    {
        $this->clearCache();
        $fdPathValue = $this->getPathValue($fdPath);
        $perms = fileperms($fdPathValue);
        switch ($perms & 0xF000) {
            case 0xC000: // 套接字
                $charPerms = 's';
                break;
            case 0xA000: // 符号链接
                $charPerms = 'l';
                break;
            case 0x8000: // regular
                $charPerms = 'r';
                break;
            case 0x6000: // 块设备
                $charPerms = 'b';
                break;
            case 0x4000: // 目录
                $charPerms = 'd';
                break;
            case 0x2000: // 字符设备
                $charPerms = 'c';
                break;
            case 0x1000: // FIFO 管道
                $charPerms = 'p';
                break;
            default: // 位置
                $charPerms = 'u';
        }
        return $charPerms;
    }

    /**
     * 获取文件大小
     * - 支持任意大小的文件
     *
     * @param string $filePath 文件的路径（或别名）
     * @return integer|false 返回文件的字节数，文件不存在时返回 false
     */
    public function getSize($filePath)
    {
        $this->clearCache();
        $filePathValue = $this->getPathValue($filePath);
        if (!file_exists($filePathValue)) {
            return false;
        }

        $size = filesize($filePathValue);

        if (false === ($file = fopen($filePathValue, 'rb'))) {
            return false;
        }

        if ($size >= 0) { //Check if it really is a small file (< 2 GB)
            if (fseek($file, 0, SEEK_END) === 0) { //It really is a small file
                fclose($file);
                return $size;
            }
        }

        //Quickly jump the first 2 GB with fseek. After that fseek is not working on 32 bit php (it uses int internally)
        $size = PHP_INT_MAX - 1;
        if (fseek($file, PHP_INT_MAX - 1) !== 0) {
            fclose($file);
            return false;
        }

        $length = 1024 * 1024;
        while (!feof($file)) { //Read the file until end
            $read = fread($file, $length);
            $size = bcadd($size, $length);
        }
        $size = bcsub($size, $length);
        $size = bcadd($size, strlen($read));

        fclose($file);
        return $size;
    }

    /**
     * 获取文件类型
     * - 不是minetype，是 file、dir 这种
     *
     * @param string $path 路径（或别名）
     * @return string 可能的值有：fifo，char，dir，block，link，file 和 unknown
     */
    public function getType($path)
    {
        $this->clearCache();
        return filetype($this->getPathValue($path));
    }

    /**
     * 是否是目录
     *
     * @param string $dirPath 目录的路径（或别名）
     * @return boolean
     */
    public function isDir($dirPath)
    {
        $this->clearCache();
        return is_dir($this->getPathValue($dirPath));
    }

    /**
     * 判断给定文件是否可执行
     *
     * @param string $filePath 文件的路径（或别名）
     * @return boolean
     */
    public function isExecutable($filePath)
    {
        $this->clearCache();
        return is_executable($this->getPathValue($filePath));
    }

    /**
     * 是否是文件
     *
     * @param string $filePath 文件的路径（或别名）
     * @return boolean
     */
    public function isFile($filePath)
    {
        $this->clearCache();
        return is_file($this->getPathValue($filePath));
    }

    /**
     * 判断给定文件是否为一个符号连接
     *
     * @param string $filePath 文件的路径（或别名）
     * @return boolean
     */
    public function isLink($filePath)
    {
        $this->clearCache();
        return is_link($this->getPathValue($filePath));
    }

    /**
     * 判断给定文件是否可读
     *
     * @param string $filePath 文件的路径（或别名）
     * @return boolean
     */
    public function isReadable($filePath)
    {
        $this->clearCache();
        return is_readable($this->getPathValue($filePath));
    }

    /**
     * 判断给定文件是否可写
     *
     * @param string $filePath 文件的路径（或别名）
     * @return boolean
     */
    public function isWritable($filePath)
    {
        $this->clearCache();
        return is_writable($this->getPathValue($filePath));
    }

    /**
     * 生成器：读取为行
     *
     * @param string $filePath 文件的路径
     * @param boolean $isRtrim 是否删除右侧空白符
     *
     * @return \Generator
     */
    public function lineGenerator($filePath, $isRtrim = true)
    {
        $filePathValue = $this->getPathValue($filePath);
        $file = new \SplFileObject($filePathValue, 'r');
        try {
            while (false === $file->eof() && ($line = $file->fgets())) {
                true === $isRtrim && $line = rtrim($line);
                yield $line;
            }
        } finally {
            $this->close($file);
        }
    }

    /**
     * 移动目录为（另一个目录）.
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromDirPath 原始目录的路径（或别名）
     * @param string $asDirPath 目标目录的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function moveDirAs($fromDirPath, $asDirPath, $isOverwrite = true)
    {
        $fromDirPathValue = $this->getPathValue($fromDirPath);
        $asDirPathValue = $this->getPathValue($asDirPath);
        if (false === $this->isDir($fromDirPathValue)) {
            return false;
        }
        if (true === $this->isDir($asDirPathValue) && false === $isOverwrite) {
            return false;
        }
        if (false === $this->isDir($asDirPathValue)) {
            $this->createDir($asDirPathValue, 0777);
        }
        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($fromDirPathValue, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($iterator as $entry) {
            /**
             * @var \SplFileInfo $entry
             */
            /**
             * @var \RecursiveDirectoryIterator $subIterator
             */
            $subIterator = $iterator->getSubIterator();
            $targetPathValue = $this->getPathValue($asDirPathValue . DIRECTORY_SEPARATOR . $subIterator->getSubPathname());
            if (false === $entry->isWritable()) {
                chmod($entry->getPathname(), 0777);
            }
            if ($entry->isFile()) {
                // 移动文件，如果目录不存在，则创建目录
                $this->createDir(dirname($targetPathValue));

                if (true === $this->isFile($targetPathValue)) {
                    if (true === $isOverwrite) {
                        $this->deleteFile($targetPathValue);
                    } else {
                        continue;
                    }
                }
                rename($entry->getPathname(), $targetPathValue);
            } else {
                // 移动空文件夹
                if (false === $this->isDir($targetPathValue)) {
                    $this->createDir($targetPathValue, 0777);
                }
                rmdir($entry->getPathname());
            }
        }

        return true;
    }

    /**
     * 移动目录到（某个目录）
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromDirPath 原始目录的路径（或别名）
     * @param string $toDirPath 目标目录的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function moveDirTo($fromDirPath, $toDirPath, $isOverwrite = true)
    {
        $toDirPath = $toDirPath . DIRECTORY_SEPARATOR . basename($fromDirPath);
        return $this->moveDirAs($fromDirPath, $toDirPath, $isOverwrite);
    }

    /**
     * 移动文件为（另一个文件）
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromFilePath 原始文件的路径（或别名）
     * @param string $asFilePath 目标文件的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function moveFileAs($fromFilePath, $asFilePath, $isOverwrite = true)
    {
        $fromFilePathValue = $this->getPathValue($fromFilePath);
        $asFilePathValue = $this->getPathValue($asFilePath);
        if (false === $this->isFile($fromFilePathValue)) {
            return false;
        }
        if ($this->isFile($asFilePathValue) && false === $isOverwrite) {
            return false;
        }
        $targetName = basename($fromFilePathValue);
        $targetDirPathValue = dirname($asFilePathValue);
        if (false === $this->isDir($targetDirPathValue)) {
            mkdir($targetDirPathValue, 0777, true);
        }
        return rename($fromFilePathValue, $targetDirPathValue . DIRECTORY_SEPARATOR . $targetName);
    }

    /**
     * 移动文件到（某个目录）
     * - 目录会被递归地创建
     * - 如果文件存在但不覆盖，会返回 false
     *
     * @param string $fromFilePath 原始文件的路径（或别名）
     * @param string $toDirPath 目标目录的路径（或别名）
     * @param boolean $isOverwrite 已存在的文件是否被覆盖，默认 true
     *
     * @return boolean
     */
    public function moveFileTo($fromFilePath, $toDirPath, $isOverwrite = true)
    {
        $asFilePath = $toDirPath . DIRECTORY_SEPARATOR . basename($fromFilePath);
        return $this->moveFileAs($fromFilePath, $asFilePath, $isOverwrite);
    }

}
