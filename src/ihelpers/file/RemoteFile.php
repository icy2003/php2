<?php
/**
 * Class RemoteFile
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */
namespace t1h0\php\ihelpers\file;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use t1h0\php\I;

/**
 * 远程文件方法类.
 */
class RemoteFile
{
    /**
     * 初始化配置
     *
     * @var array{bufferSize?:integer}
     */
    protected $_config = [];

    /**
     * 单例对象
     *
     * @var static
     */
    protected static $_instance;

    /**
     * 构造函数
     */
    protected function __construct()
    {
    }

    /**
     * 克隆函数
     *
     * @return void
     */
    protected function __clone()
    {
    }

    /**
     * 创建文件对象的单例.
     *
     * @param array $config
     *  - bufferSize: 缓冲区大小，默认 4096
     *
     * @return static
     */
    public static function instance($config = [])
    {
        if (!static::$_instance instanceof static ) {
            static::$_instance = new static();
        }
        static::$_instance->_config = array_merge(static::$_instance->_config, [
            'bufferSize' => I::get($config, 'bufferSize', 4096),
        ]);

        return static::$_instance;
    }

    /**
     * 返回请求头的响应
     *
     * @param string $url 远程文件的地址
     * @return \Psr\Http\Message\ResponseInterface|null
     */
    private function __getHeadResponse($url)
    {
        // 发送HEAD请求
        $client = new Client();
        try {
            return $client->head($url);
        } catch (GuzzleException $e) {
            // 处理异常
            return false;
        }
    }

    /**
     * 获取远程文件大小
     * - 需头部设置 Content-Length
     *
     * @param string $url 远程文件的地址
     * @param \Psr\Http\Message\ResponseInterface|null $response 头部响应对象
     * @return integer|false 返回文件的字节数，文件不存在时返回 false
     */
    public function getSize($url, $response = null)
    {
        if (null === $response) {
            $response = $this->__getHeadResponse($url);
        }
        if (false === $response) {
            return 0;
        } else {
            /**
             * @var \Psr\Http\Message\ResponseInterface $response
             */
            return intval($response->getHeaderLine('Content-Length'));
        }
    }

    /**
     * 获取远程文件Mine-Type
     * - 需头部设置 Content-Type
     *
     * @param string $url 远程文件的地址
     * @param \Psr\Http\Message\ResponseInterface|null $response 头部响应对象
     * @return integer|false 返回文件的 Mine-Type，文件不存在时返回 false
     */
    public function getMinetype($url, $response = null)
    {
        if (null === $response) {
            $response = $this->__getHeadResponse($url);
        }
        if (false === $response) {
            return false;
        } else {
            /**
             * @var \Psr\Http\Message\ResponseInterface $response
             */
            return $response->getHeaderLine('Content-Type');
        }
    }

    /**
     * 获取远程文件最后更新时间
     * - 需头部设置 Last-Modified
     *
     * @param string $url 远程文件的地址
     * @param \Psr\Http\Message\ResponseInterface|null $response 头部响应对象
     * @return integer|false 返回文件的最后更新时间，文件不存在时返回 false
     */
    public function getModifyTime($url, $response = null)
    {
        if (null === $response) {
            $response = $this->__getHeadResponse($url);
        }
        if (false === $response) {
            return false;
        } else {
            /**
             * @var \Psr\Http\Message\ResponseInterface $response
             */
            return strtotime($response->getHeaderLine('Last-Modified'));
        }
    }

    /**
     * 生成器：从远程文件下载为片段
     *
     * @param string $url 文件URL
     * @param integer|null $index 分片索引，默认不给的话是返回全部
     * @param integer $chunkSize 分片大小，默认 1024*1024
     * @return \Generator<integer, string> 键是分片索引，值是对应的二进制数据
     */
    public function downloadGenerator($url, $index = null, $chunkSize = 1024 * 1024)
    {
        // 解析配置项
        $response = $this->__getHeadResponse($url);
        $sizeTotal = $this->getSize($url, $response);
        $chunkTotal = ceil($sizeTotal / $chunkSize);
        function downloadPart($url, $index, $chunkSize, $sizeTotal)
        {
            $client = new Client();
            $start = $index * $chunkSize; // 计算分片起始位置
            $end = min(($index + 1) * $chunkSize - 1, $sizeTotal - 1); // 计算分片结束位置，并确保不超过文件总大小

            try {
                // 发送范围请求来获取文件的一个部分
                $rangeResponse = $client->get($url, [
                    'headers' => [
                        'Range' => "bytes={$start}-{$end}",
                    ],
                    'stream' => true, // 获取响应体流
                ]);

                // 获取响应体的流
                $stream = $rangeResponse->getBody();

                // 将流yield出去
                yield $stream;
            } catch (GuzzleException $e) {
                // 处理异常，例如范围无效或服务器不支持范围请求
                // echo 'Error downloading file part: ' . $e->getMessage();
            }
        }
        for ($i = 0; $i < $chunkTotal; $i++) {
            if (null !== $index) {
                if ($index !== $i) {
                    continue;
                }
            }
            foreach (downloadPart($url, $i, $chunkSize, $sizeTotal) as $stream) {
                $data = '';
                while (false === $stream->eof()) {
                    $data .= $stream->read($this->_config['bufferSize']);
                }
                yield $i => $data;
            }
        }
    }

}
