<?php
/**
 * Class Unicode
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\ihelpers;

/**
 * Unicode 编码/解码
 */
class Unicode
{
    /**
     * Unicode 编码
     *
     * @param string $string
     *
     * @return string
     */
    public static function encode($string)
    {
        return trim(json_encode($string), '"');
    }

    /**
     * Unicode 解码
     *
     * @param string $string
     *
     * @return string
     */
    public static function decode($string)
    {
        return implode('', (array) Json::decode('["' . $string . '"]'));
    }
}
