<?php
/**
 * Class Tree
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */
namespace t1h0\php\ihelpers;

use t1h0\php\I;

/**
 * 树操作
 */
class Tree
{
    /**
     * 展开一棵树
     *
     * - 展开后为一维数组，并且靠后的元素的 pid 对应 id 的元素一定在靠前的元素里存在
     *
     * @param array $array
     * @param string $rootId 根节点 ID
     * @param string $idName ID 名
     * @param string $pidName PID 名
     *
     * @return array
     */
    public static function expand($array, $rootId = '0', $idName = 'id', $pidName = 'pid')
    {
        $array = ArrayStatic::indexBy($array, $idName);
        $array2 = ArrayStatic::indexBy($array, $pidName, true);
        $pidArray = [$rootId];
        $return = [];
        while (true) {
            $temp = [];
            foreach ($pidArray as $pid) {
                $rows = (array) I::get($array2, $pid, []);
                if (empty($rows)) {
                    continue;
                }
                $return = ArrayStatic::merge($return, $rows);
                $temp = ArrayStatic::merge($temp, ArrayStatic::column($rows, $idName));
            }
            if (empty($temp)) {
                break;
            }
            $pidArray = $temp;
        }
        return $return;
    }
}
