<?php
/**
 * Class Http
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2017, t1h0
 */

namespace t1h0\php\ihelpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use t1h0\php\I;
use t1h0\php\ihelpers\file\LocalFile;

/**
 * 用 guzzlehttp 封装的简单的方法，更复杂的请使用 guzzlehttp
 */
class Http
{
    /**
     * 发送 GET 请求
     *
     * @param string $url 请求地址
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     *
     * @return string
     */
    public static function get($url, $getParams = [], $options = [])
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $response = $client->request('GET', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
        ]);
        return $response->getBody()->getContents();
    }

    /**
     * 发送异步的 GET 请求
     *
     * @param string $url 请求地址
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     * @param callback $successCall 成功时的回调 function(string $result, ResponseInterface $response) $result 是内容，$response 是结果对象
     * @param callback $errorCall 失败时的回调 function($e) $e 是结果对象
     *
     * @return void
     */
    public static function getAsync($url, $getParams = [], $options = [], $successCall = null, $errorCall = null)
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $promise = $client->requestAsync('GET', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
        ]);
        $promise->then(function (ResponseInterface $response) use ($successCall) {
            I::call($successCall, [$response->getBody()->getContents(), $response]);
        }, function (RequestException $e) use ($errorCall) {
            I::call($errorCall, [$e]);
        });
    }

    /**
     * 发送 application/x-www-form-urlencoded POST 请求
     *
     * @param string $url 请求地址
     * @param array $postParams POST 参数
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     *
     * @return string
     */
    public static function post($url, $postParams = [], $getParams = [], $options = [])
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $response = $client->request('POST', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
            'form_params' => $postParams,
        ]);
        return $response->getBody()->getContents();
    }

    /**
     * 发送异步的 application/x-www-form-urlencoded POST 请求
     *
     * @param string $url 请求地址
     * @param array $postParams POST 参数
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     * @param callback $successCall 成功时的回调 function($result, $response) $result 是内容，$response 是结果对象
     * @param callback $errorCall 失败时的回调 function($e) $e 是结果对象
     *
     * @return void
     */
    public static function postAsync($url, $postParams = [], $getParams = [], $options = [], $successCall = null, $errorCall = null)
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $promise = $client->requestAsync('POST', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
            'form_params' => $postParams,
        ]);
        $promise->then(function (ResponseInterface $response) use ($successCall) {
            I::call($successCall, [$response->getBody()->getContents(), $response]);
        }, function (RequestException $e) use ($errorCall) {
            I::call($errorCall, [$e]);
        });
    }

    /**
     * 发送 multipart/form-data 表单文件请求
     *
     * @param string $url 请求地址
     * @param array $fileMap 文件映射：文件name=>文件（文件路径或别名、资源）
     * @param array $postParams POST 参数
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     *
     * @return string
     */
    public static function postFile($url, $fileMap, $postParams = [], $getParams = [], $options = [])
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $params = [];
        foreach ($fileMap as $name => $file) {
            $params[] = ['name' => $name, 'contents' => is_string($file) ? fopen((new LocalFile)->getPathValue($file), 'r') : $file];
        }
        foreach ($postParams as $postName => $postValue) {
            $params[] = ['name' => $postName, 'contents' => $postValue];
        }
        $response = $client->request('POST', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
            'multipart' => $params,
        ]);
        return $response->getBody()->getContents();
    }

    /**
     * 发送异步的 multipart/form-data 表单文件请求
     *
     * @param string $url 请求地址
     * @param array $fileMap 文件映射：文件name=>文件（文件路径或别名、资源）
     * @param array $postParams POST 参数
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     * @param callback $successCall 成功时的回调 function($result, $response) $result 是内容，$response 是结果对象
     * @param callback $errorCall 失败时的回调 function($e) $e 是结果对象
     *
     * @return void
     */
    public static function postFileAsync($url, $fileMap, $postParams = [], $getParams = [], $options = [], $successCall = null, $errorCall = null)
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        foreach ($fileMap as $name => $file) {
            $params[] = ['name' => $name, 'contents' => is_string($file) ? fopen((new LocalFile)->getPathValue($file), 'r') : $file];
        }
        foreach ($postParams as $postName => $postValue) {
            $params[] = ['name' => $postName, 'contents' => $postValue];
        }
        $promise = $client->requestAsync('POST', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
            'multipart' => $params,
        ]);
        $promise->then(function (ResponseInterface $response) use ($successCall) {
            I::call($successCall, [$response->getBody()->getContents(), $response]);
        }, function (RequestException $e) use ($errorCall) {
            I::call($errorCall, [$e]);
        });
    }

    /**
     * 发送文本 POST 请求
     *
     * @param string $url 请求地址
     * @param string $body POST 文本
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     *
     * @return string
     */
    public static function body($url, $body = '', $getParams = [], $options = [])
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $response = $client->request('POST', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
            'body' => $body,
        ]);
        return $response->getBody()->getContents();
    }

    /**
     * 发送异步的文本 POST 请求
     *
     * @param string $url 请求地址
     * @param string $body POST 文本
     * @param array $getParams GET 参数
     * @param array $options 额外参数，默认不检测 https
     * @param callback $successCall 成功时的回调 function($result, $response) $result 是内容，$response 是结果对象
     * @param callback $errorCall 失败时的回调 function($res) $res 是结果对象
     *
     * @return void
     */
    public static function bodyAsync($url, $body = '', $getParams = [], $options = [], $successCall = null, $errorCall = null)
    {
        list($url, $query) = self::__parseUrl($url);
        $client = new Client(ArrayStatic::merge(['verify' => false], $options));
        $promise = $client->requestAsync('POST', $url, [
            'query' => ArrayStatic::merge($query, $getParams),
            'body' => $body,
        ]);
        $promise->then(function (ResponseInterface $response) use ($successCall) {
            I::call($successCall, [$response->getBody()->getContents(), $response]);
        }, function (RequestException $e) use ($errorCall) {
            I::call($errorCall, [$e]);
        });
    }

    /**
     * 解析 URL
     *
     * @param string $url
     *
     * @return array
     */
    private static function __parseUrl($url)
    {
        $parts = parse_url($url);
        $url = $parts['scheme'] . '://' . $parts['host'];
        if (isset($parts['port'])) {
            $url .= ':' . $parts['port'];
        }
        $url .= $parts['path'];
        if (isset($parts['query'])) {
            return [$url, ArrayStatic::explode('=', explode('&', $parts['query']))];
        } else {
            return [$url, []];
        }
    }
}
