<?php
/**
 * Class Bitmap
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */

namespace t1h0\php\ihelpers;

/**
 * 位图算法查找缺失分片
 * - 该类旨在使用位图查询缺失分片的索引
 *
 * 假设位图最大索引（maxIndex）为11，位元素长（bitElementLength）为5时，将会初始化如下位图：
 * - 如果构造函数给了位图元素索引数组（elementIndexs）为`[1]`，那么只会初始化第二行的位图
 *
 * ```bitmap
 *  00000
 *  00000
 *  00000
 * ```
 * 其中：
 * - 位图元素（bitElement）：每一行为一个元素，对应索引从0开始，称为位图元素索引（elementIndex），如第二行的位图元素索引为1
 * - 位图索引（或分片索引）（shardIndex）：整个位图对应的位置，如第一行第一个为0，第三行最后一个为14
 * - 位索引（bitIndex）：只某一行对应的索引，如第三行第一个，位图索引为10，但位索引为0
 */
class Bitmap
{
    // 位图数组，表示分片的状态
    private $__bitElements;
    // 每个字符串元素表示的分片数量（位数）
    private $__bitElementLength;
    // 位图最大索引值
    private $__maxIndex;

    /**
     * 初始化 bitmap
     *
     * @param integer $maxIndex 位图最大索引=分片数量-1
     * @param integer $bitElementLength 位元素长度=32
     * @param integer[] $elementIndexs 位图元素索引数组。有值时，bitmap只会初始化索引对应的元素，为空时初始化整个bitmap
     */
    public function __construct($maxIndex, $bitElementLength = 32, $elementIndexs = [])
    {
        $this->__maxIndex = $maxIndex;
        $this->__bitElementLength = $bitElementLength;
        // 初始化位图数组
        if ($elementIndexs) {
            foreach ($elementIndexs as $elementIndex) {
                $this->__bitElements[$elementIndex] = str_repeat('0', $bitElementLength);
            }
        } else {
            // 计算需要的字符串元素数量
            $elementCount = ceil(($maxIndex + 1) / $bitElementLength);
            $this->__bitElements = array_fill(0, $elementCount, str_repeat('0', $bitElementLength));
        }
    }

    /**
     * 设置指定位图索引对应的分片为已存在
     *
     * @param integer $shardIndex 分片的位图索引
     * @return integer 被设置的位图元素索引
     */
    public function setShard($shardIndex)
    {
        $elementIndex = intval($shardIndex / $this->__bitElementLength); // 计算位图元素索引
        $bitIndex = $shardIndex % $this->__bitElementLength; // 计算位索引
        // 如果没有初始化过，此时初始化
        if (!array_key_exists($elementIndex, $this->__bitElements)) {
            $this->__bitElements[$elementIndex] = str_repeat('0', $this->__bitElementLength);
        }
        // 使用 substr_replace 设置对应位为 '1'
        $this->__bitElements[$elementIndex] = substr_replace($this->__bitElements[$elementIndex], '1', $bitIndex, 1);

        return $elementIndex;
    }

    /**
     * 使用数组批量设置已有分片
     *
     * @param integer[] $shardIndexs 分片的位图索引的数组
     * @return integer[] 被设置的位图元素索引数组
     */
    public function setShards($shardIndexs)
    {
        $elementIndexs = [];
        foreach ($shardIndexs as $shardIndex) {
            $elementIndexs[] = $this->setShard($shardIndex);
        }
        return $elementIndexs;
    }

    /**
     * 直接设置位图元素的值
     *
     * @param integer $elementIndex 位图元素索引
     * @param string $elementValue 位图元素值
     * @return void
     */
    public function setBitElement($elementIndex, $elementValue)
    {
        $this->__bitElements[$elementIndex] = $elementValue;
    }

    /**
     * 获取位图元素的值
     *
     * @param integer $elementIndex 位图元素索引
     * @return string
     */
    public function getBitElement($elementIndex)
    {
        return array_key_exists($elementIndex, $this->__bitElements) ? $this->__bitElements[$elementIndex] : '';
    }

    /**
     * 获取位图元素数组
     *
     * @param integer|null $elementIndex 位图元素索引
     * @return array $elementIndex 有值时，返回对应索引的单元素数组，为null时返回全部已经设置的元素数组
     */
    public function getBitElements($elementIndex = null)
    {
        if (null !== $elementIndex) {
            if (array_key_exists($elementIndex, $this->__bitElements)) {
                $bits = [$elementIndex => $this->__bitElements[$elementIndex]];
            } else {
                return [];
            }
        } else {
            $bits = $this->__bitElements;
        }
        return $bits;
    }

    /**
     * 获取缺失的所有分片的索引数组
     *
     * @param integer|null $elementIndex 位图元素索引，当设置该值时，只在对应的位图元素上找缺失分片
     * @return array 缺失的分片的索引数组
     */
    public function getMissingShards($elementIndex = null)
    {
        $missingShards = [];
        $bitElements = $this->getBitElements($elementIndex);
        foreach ($bitElements as $bitElement) {
            for ($i = 0; $i < strlen($bitElement); $i++) {
                $shardIndex = $i + (array_search($bitElement, $bitElements) * strlen($bitElement));
                if ($shardIndex <= $this->__maxIndex && $bitElement[$i] === '0') {
                    $missingShards[] = $shardIndex;
                }
            }
        }
        return $missingShards;
    }

    /**
     * 获取第一个缺失的分片索引
     *
     * @param integer|null $elementIndex 位图元素索引，当设置该值时，只在对应的位图元素上找缺失分片
     * @return integer|null 第一个缺失的分片索引，如果所有分片都存在则返回null
     */
    public function getFirstMissingShard($elementIndex = null)
    {
        $bitElements = $this->getBitElements($elementIndex);
        foreach ($bitElements as $bitElement) {
            for ($i = 0; $i < strlen($bitElement); $i++) {
                $shardIndex = $i + (array_search($bitElement, $bitElements) * strlen($bitElement));
                if ($shardIndex > $this->__maxIndex) {
                    // 超出最大索引，结束循环
                    break;
                }
                if ($bitElement[$i] === '0') {
                    // 找到第一个缺失的分片
                    return $shardIndex;
                }
            }
        }
        // 所有分片都存在
        return null;
    }
}
