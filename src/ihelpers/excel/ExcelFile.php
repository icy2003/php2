<?php
/**
 * Class ExcelFile
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */
namespace t1h0\php\ihelpers\excel;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use t1h0\php\I;
use t1h0\php\ihelpers\ArrayStatic;
use t1h0\php\ihelpers\File;
use t1h0\php\ihelpers\Strings;
use t1h0\php\ihelpers\Type;

/**
 * Excel 文件操作类.
 */
class ExcelFile
{

    /**
     * @return array
     */
    private static function __getSheetAndRange($entry)
    {

        $localFile = File::local();
        if (is_array($entry)) {
            list($filePath, $sheetName, $range) = ArrayStatic::lists($entry, 3);
        } else {
            $sheetName = null;
        }
        // 读取文件
        $filePathValue = $localFile->getPathValue($filePath);
        $reader = IOFactory::createReaderForFile($filePathValue);
        $spreadsheet = $reader->load($filePathValue);
        if (null === $sheetName) {
            $worksheet = $spreadsheet->getActiveSheet();
        } else {
            if (Type::isNumber($sheetName)) {
                $worksheet = $spreadsheet->getSheet($sheetName);
            } else {
                $worksheet = $spreadsheet->getSheetByName($sheetName);
            }
        }
        $maxColumn = $worksheet->getHighestColumn();
        $maxColumnIndex = Coordinate::columnIndexFromString($maxColumn);

        $maxRowIndex = $worksheet->getHighestRow();
        if ($range) {
            $range = Strings::replace($range, ['$' => '']);
            try {
                $rangeBoundaries = Coordinate::rangeBoundaries($range);
            } catch (\Exception $e) {
                $rangeBoundaries = null;
            }
        } else {
            $rangeBoundaries = null;
        }
        // 开始行列索引
        $startRowIndex = $rangeBoundaries ? $rangeBoundaries[0][1] : 1;
        $startColumnIndex = $rangeBoundaries ? $rangeBoundaries[0][0] : 1;
        // 结束行列索引
        $endRowIndex = $rangeBoundaries ? min($maxRowIndex, $rangeBoundaries[1][1]) : $maxRowIndex;
        $endColumnIndex = $rangeBoundaries ? min($maxColumnIndex, $rangeBoundaries[1][0]) : $maxColumnIndex;
        return [$worksheet, [[$startColumnIndex, $startRowIndex], [$endColumnIndex, $endRowIndex]]];
    }

    /**
     * 生成器：遍历xlsx（xlsx、xls、csv、ods等）的行
     *  - 如果数据不包含表头，则不该使用表头相关的参数：headerFilter、headerName
     *  - 13万个单元格（1万行16列带空单元格）耗时20s左右
     *  - 如需更高性能，请用box\spout或openspout
     *
     * @param string|array $entry 文件的路径（或别名）以及选定工作簿和范围
     *      1. 字符串：指定文件，并且选定当前工作簿和全部内容
     *      2. 数组：[文件, 工作簿, 范围]
     *          - 如果工作簿是数字（严格的如1，而非'1'），表示工作簿索引，如果工作簿是字符，表示工作簿名
     *          - 单元格范围格式为 A1:D3，可加$可不加，忽略大小写
     *          - 选定范围超出实际，则以实际为准（假设表有99行，但是范围设置为200行，最终只会遍历99行）
     * @param array{headerFilter: callable, rowFilter: callable, cellFilter: callable} $filters 过滤器
     *      - headerFilter($cell: \PhpOffice\PhpSpreadsheet\Cell\Cell)：表头过滤函数，如果设置了，true的值才会被遍历
     *      - rowFilter($cell: \PhpOffice\PhpSpreadsheet\Cell\Cell, $headerName)：行过滤函数，$headerName为表头的值
     * @param callable|null $formatter ($cell: \PhpOffice\PhpSpreadsheet\Cell\Cell, $headerName) 格式化函数，默认null，返回计算值
     * @return \Generator 行号=>[列号=>计算值（格式化值）]
     */
    public static function rowGenerator($entry, $filters = [], $formatter = null)
    {
        /**
         * @var \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $worksheet
         */
        list($worksheet, $rangeBoundaries) = self::__getSheetAndRange($entry);
        $columnFilterArray = $rowFilterArray = $headerNames = [];
        // 遍历指定范围
        for ($rowIndex = $rangeBoundaries[0][1]; $rowIndex <= $rangeBoundaries[1][1]; $rowIndex++) {
            // 过滤结果
            for ($colomnIndex = $rangeBoundaries[0][0]; $colomnIndex <= $rangeBoundaries[1][0]; $colomnIndex++) {
                $columnName = Coordinate::stringFromColumnIndex($colomnIndex);
                $cell = $worksheet->getCell($columnName . $rowIndex);
                // 遍历第一行
                if ($rowIndex === $rangeBoundaries[0][1]) {
                    // 记录表头
                    $headerNames[$colomnIndex] = $cell->getCalculatedValue();
                    // 找出需要过滤的列
                    if (I::call(I::get($filters, 'headerFilter'), [$cell])) {
                        $columnFilterArray[] = $colomnIndex;
                    }
                }
                // 每一行，找出需要过滤的行
                if (I::call(I::get($filters, 'rowFilter'), [$cell, $headerNames[$colomnIndex]])) {
                    if (false === in_array($rowIndex, $rowFilterArray)) {
                        $rowFilterArray[] = $rowIndex;
                    }
                }

            }

            // 跳过行：如果设置了 rowFilter，并且rowIndex不在过滤结果里，跳过该行
            if (null !== I::get($filters, 'rowFilter')) {
                if (false === in_array($rowIndex, $rowFilterArray)) {
                    continue;
                }
            }
            // 生成单元格
            $cells = [];
            for ($colomnIndex = $rangeBoundaries[0][0]; $colomnIndex <= $rangeBoundaries[1][0]; $colomnIndex++) {
                if ($columnFilterArray) {
                    if (false === in_array($colomnIndex, $columnFilterArray)) {
                        continue;
                    }
                }
                $columnName = Coordinate::stringFromColumnIndex($colomnIndex);
                $cell = $worksheet->getCell($columnName . $rowIndex);
                // 产出当前单元格的值
                $cells[$columnName] = null === $formatter ? $cell->getCalculatedValue() : I::call($formatter, [$cell, $headerNames[$colomnIndex]]);
            }
            if ($cells) {
                yield $rowIndex => $cells;
            } else {
                yield [];
            }
        }
    }

    public static function writeTo($data, $filePath)
    {
        $spreadsheet = new Spreadsheet();

    }

    // public static function toPDF($from, $to = null)
    // {
    //     $localFile = File::local();
    //     $filePathValue = $localFile->getPathValue($from);
    //     $reader = IOFactory::createReaderForFile($filePathValue);
    //     $spreadsheet = $reader->load($filePathValue);
    //     $writer = IOFactory::createWriter($spreadsheet, 'Mpdf');
    //     $writer->save($to);
    // }

    // public static function toXlsx($fromPath, $toPath = null)
    // {
    //     $local = File::local();
    //     $fromPathValue = $local->getPathValue($fromPath);
    //     if (null === $toPath) {
    //         $toPath = dirname($fromPathValue) . $local->getFileName($fromPathValue) . '.xlsx';
    //     }
    //     $toPathValue = $local->getPathValue($toPath);
    //     $fromType = IOFactory::identify($fromPathValue);
    //     $reader = IOFactory::createReader($fromType);
    //     $spreadsheet = $reader->load($fromPathValue);
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save($toPathValue);
    //     return true;
    // }

    // /**
    //  * 使用 Box\Spout 流式写入 XLSX 文件。
    //  *  - @depends box\spout
    //  *
    //  * @param iterable<array> $data 包含要写入 XLSX 文件的单元格数据的数组或生成器，每个元素是一个数组代表一行数据。
    //  * @param string $filePath 要写入的 XLSX 文件的路径。
    //  * @throws \Box\Spout\Writer\Exception\IOException 如果写入过程中发生 I/O 错误。
    //  * @return void
    //  */
    // public static function writeXlsxWithSpout(string $filePath, iterable $data): void
    // {
    //     $localFile = File::local();
    //     $filePathValue = $localFile->getPathValue($filePath);
    //     $localFile->createDir(dirname($filePathValue));
    //     $writer = \Box\Spout\Writer\Common\Creator\WriterEntityFactory::createXLSXWriter();
    //     $writer->openToFile($filePathValue);
    //     $style = (new \Box\Spout\Writer\Common\Creator\Style\StyleBuilder())
    //         ->setCellAlignment(\Box\Spout\Common\Entity\Style\CellAlignment::CENTER)
    //         ->build();
    //     foreach ($data as $cellValues) {
    //         $cells = [];
    //         foreach ($cellValues as $cellValue) {
    //             $cells[] = \Box\Spout\Writer\Common\Creator\WriterEntityFactory::createCell($cellValue);
    //         }
    //         $row = \Box\Spout\Writer\Common\Creator\WriterEntityFactory::createRow($cells);
    //         $row->setStyle($style);
    //         $writer->addRow($row);
    //     }

    //     $writer->close();
    // }

    /**
     * 使用 PhpSpreadsheet 进行流式写入 XLSX 文件。
     *
     * @param iterable<array<mixed>> $data 包含要写入 XLSX 文件的单元格数据的数组或生成器。
     * @param string $outputPath 要写入的 XLSX 文件的路径。
     * @throws \PhpOffice\PhpSpreadsheet\Exception 如果写入过程中发生错误。
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception 如果写入器创建失败。
     * @return void
     */
    public static function writeXlsx($data, $outputPath)
    {
        // 创建一个新的电子表格对象
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        // 遍历数据并写入电子表格
        $row = 1;
        foreach ($data as $item) {
            $column = 1;
            foreach ($item as $value) {
                $worksheet->setCellValue([$column, $row], $value);
                $column++;
            }
            $row++;
        }

        // 创建一个Xlsx写入器对象
        $writer = new Xlsx($spreadsheet);

        // 启用流式写入
        $writer->setPreCalculateFormulas(false);
        $writer->save($outputPath);
    }
}
