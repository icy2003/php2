<?php
/**
 * Class Type
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */
namespace t1h0\php\ihelpers;

/**
 * 类型相关的类
 */
class Type
{
    /**
     * 判断变量是不是严格的数字
     *
     * @param $mixed $data
     * @return boolean
     */
    public static function isNumber($data, $isStrict = true)
    {
        if (is_numeric($data)) {
            if (false === $isStrict) {
                return true;
            }
            if (false === is_string($data)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断变量是不是日期
     *
     * @param $mixed $data
     * @return boolean
     */
    public static function isDatetime($data)
    {
        if (is_string($data)) {
            return false !== strtotime($data);
        }
        return false;
    }

}
