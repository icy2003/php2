<?php

/**
 * Class File
 *
 * @link https://www.t1h0.com/
 * @author t1h0 <2317216477@qq.com>
 * @copyright Copyright (c) 2024, t1h0
 */
namespace t1h0\php\ihelpers;

use t1h0\php\ihelpers\file\LocalFile;
use t1h0\php\ihelpers\file\RemoteFile;

/**
 * 文件方法集合.
 */
class File
{
    /**
     * 本地文件方法单例.
     *
     * @param \t1h0\php\ihelpers\file\LocalFile::$_config $config
     *
     * @return \t1h0\php\ihelpers\file\LocalFile
     */
    public static function local($config = null)
    {
        return LocalFile::instance($config);
    }

    /**
     * 远程文件方法单例.
     *
     * @param \t1h0\php\ihelpers\file\RemoteFile::$_config $config
     *
     * @return \t1h0\php\ihelpers\file\RemoteFile
     */
    public static function remote($config = null)
    {
        return RemoteFile::instance($config);
    }

    /**
     * 服务器端给客户端发送文件
     *
     * @param string|array $filePaths 文件分片的路径（或别名）数组或文件的路径（或别名）
     * @param string $fileName 下载的文件名（需带后缀）
     * @param boolean $useChunked 服务器端是否使用了分片
     * @param integer $speedLimit 下载限速，默认不限速，单位字节
     * @return void
     */
    public static function send($filePaths, $fileName, $useChunked = false, $speedLimit = null)
    {
        $localFile = LocalFile::instance();
        try {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $fileName);
            if ($useChunked) {
                // 使用 Transfer-Encoding: chunked
                header('Transfer-Encoding: chunked');
            } else {
                if (is_string($filePaths)) {
                    $filePaths = [$filePaths];
                }
                $fileSize = 0;
                foreach ($filePaths as $filePath) {
                    $filePathValue = $localFile->getPathValue($filePath);
                    $fileSize += $localFile->getSize($filePathValue);
                }
                header('Content-Length: ' . $fileSize);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Expires: 0');
            ob_end_clean(); // 关闭输出缓冲，确保数据直接发送到浏览器
            flush(); // 刷新输出缓冲区
            if (is_array($filePaths)) { // 如果是分片数组
                $chunkCount = count($filePaths);
                $chunkIndex = 0;
                while ($chunkIndex < $chunkCount) {
                    $chunkPathValue = $localFile->getPathValue($filePaths[$chunkIndex]);
                    $chunk = fopen($chunkPathValue, 'rb');
                    $chunkData = fread($chunk, $localFile->getSize($chunkPathValue));
                    if ($chunkData === false) {
                        break; // 读取错误或已到达文件末尾
                    }
                    if ($useChunked) {
                        echo dechex(strlen($chunkData)) . "\r\n"; // 发送chunk大小（十六进制）
                        echo $chunkData . "\r\n"; // 发送chunk内容
                    } else {
                        echo $chunkData;
                    }
                    ob_flush(); // 刷新输出缓冲区
                    flush(); // 确保数据发送到浏览器
                    // 引入延迟以限制下载速度
                    null !== $speedLimit && usleep(ceil(strlen($chunkData) / $speedLimit * 1000));
                    $chunkIndex++;
                }
            } else { // 如果是单个文件
                $filePathValue = $localFile->getPathValue($filePaths);
                $file = fopen($filePathValue, 'rb');
                // 发送整个文件
                while (!feof($file)) {
                    $chunkData = fread($file, 4096);
                    if ($useChunked) {
                        echo dechex(4096) . "\r\n";
                        echo $chunkData . "\r\n";
                    } else {
                        echo $chunkData; // 一次读取并发送一部分数据
                    }
                    ob_flush(); // 刷新输出缓冲区
                    flush(); // 确保数据被发送到浏览器
                    // 引入延迟以限制下载速度
                    null !== $speedLimit && usleep(ceil(4096 / $speedLimit * 1000));
                }
                fclose($file); // 关闭当前分片（如果它是文件资源）
            }
            if ($useChunked) {
                // 发送结束信号（分块大小为0）
                echo "0\r\n\r\n";
                ob_flush();
                flush();
            }
        } catch (\Exception $e) {
            header($e->getMessage());
        } finally {
            die;
        }
    }

}
